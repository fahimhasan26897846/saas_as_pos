<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = ['user_id','name','code','base','value'];
    protected $table = 'unit';
}
