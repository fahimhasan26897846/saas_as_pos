<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Invoice;
use App\Product;
use App\Suppliers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Sentinel;

class DashboardController extends Controller
{
    private $userObj;

    public function showDashboard(Request $request)
    {
        $this->userObj = isset($this->userObj) ?: UserPermissionController::userInfo();

        switch (session('login_as')) {
            case 'admin':
                return view('admin.index')->with('thisUser', $this->userObj);
            case 'user':
                $customer = Customer::where('user_id',Sentinel::getUser()->id)->get();
                $product = Product::where('user_id',Sentinel::getUser()->id)->get();
                $invoice = Invoice::where('user_id',Sentinel::getUser()->id)->get();
                $suppliers = Suppliers::where('user_id',Sentinel::getUser()->id)->get();
                return view('index')->with('thisUser', $this->userObj)
                    ->with('customer',$customer)
                    ->with('product',$product)
                    ->with('invoice',$invoice)
                    ->with('suppliers',$suppliers);
            default:
                $this->userObj->destroyUserSession($request);
                return redirect('login')->with('error', 'Something went wrong please login again');
        }
    }
}
