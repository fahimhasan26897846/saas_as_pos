<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\UserPermissionController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;

class AdminController extends Controller
{
    private $userObj;

    public function showAdminList()
    {
        $this->userObj = isset($this->userObj) ?: UserPermissionController::userInfo();

        if (!$this->userObj->hasAccessTo('view_admin')) {
            return redirect()->back();
        }

        $data = [
            'thisUser'      => $this->userObj,
            'admins'        => $this->adminList(),
        ];

        return view('admin.admin-list')->with($data);
    }

    private function adminList()
    {
        return Admin::getAdminList();
    }

    public function showProfile()
    {
        $this->userObj = isset($this->userObj) ?: UserPermissionController::userInfo();

        if (!$this->userObj->hasAccessTo('view_admin')) {
            return redirect()->back();
        }

        $data = [
            'thisUser'      => $this->userObj,
            'admins'        => $this->adminList(),
        ];

        return view('admin.profile-admin')->with($data);
    }

     public function editProfile()
    {
        $this->userObj = isset($this->userObj) ?: UserPermissionController::userInfo();

        if (!$this->userObj->hasAccessTo('view_admin')) {
            return redirect()->back();
        }

        $data = [
            'thisUser'      => $this->userObj,
            'admins'        => $this->adminList(),
        ];

        return view('admin.edit-admin')->with($data);
    }

}
