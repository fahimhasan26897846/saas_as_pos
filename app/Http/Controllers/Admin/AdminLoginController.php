<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Reminder;
use Mail;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

class AdminLoginController extends Controller
{
    public function showLogin()
    {
        return view('admin.login');
    }

    private function authenticateUser(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        $user = Sentinel::findByCredentials(['email' => $request->email]);
        $role = $user->roles()->first()->slug;

        if ($role !== 'Admin' && $role !== 'SuperAdmin') {
            return false;
        }

        if (isset($request->remember_me) && $request->remeber_me === 'on') {
            return Sentinel::authenticateAndRemember($credentials);
        }

        return Sentinel::authenticate($credentials);
    }

    public function postLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        try {
            if (!($user = $this->authenticateUser($request))) {
                return redirect()->back()->with(['error' => 'Wrong login information.']);
            }


            $user_role = $this->getUserRole($user);
            if ($user_role !== 'Admin' && $user_role !== 'SuperAdmin') {
                Sentinel::logout();

                return redirect()->back()->with(['error' => 'Wrong login information.']);
            }
            $request->session()->put('login_as', 'admin');
            return redirect('/dashboard')->with(['success' => 'Welcome to Admin Panel']);

        } catch(ThrottlingException $e) {
            $delay = $e->getDelay();
            return redirect()->back()->with(['error' => "You can login after $delay seconds."]);
        } catch(NotActivatedException $e) {
            return redirect()->back()->with(['error' => 'Account not activated yet.']);
        }
    }

    public function forgotPassword(Request $request)
    {
        $user = Sentinel::findByCredentials(['email' => $request->email]);

        if (count($user) === 1) {
            $reminder = Reminder::exists($user) ?: Reminder::create($user);
            $this->sendEmail($user, $reminder->code);
        }

        return redirect()->back()->with(['success' => 'Reset code was sent to your email']);
    }

    public function showResetPassword(Request $request) {
        $user = Sentinel::findById($request->id);

        if (!$user) {
            abort(404);
        }

        if(($reminder = Reminder::exists($user)) && $request->code === $reminder->code) {
            return view('admin.reset-password')->with(['id' => $request->id, 'code' => $request->code]);
        }

        return redirect('/admin/login');
    }

    private function sendEmail($user , $code)
    {
        Mail::send('admin.emails.reset-password', [
            'id' => $user->id,
            'code' => $code
        ], function($message) use ($user) {
            $message->to($user->email);
            $message->subject("Hello $user->first_name, reset your password");
        });
    }

    public function postResetPassword(Request $request, $id, $resetCode)
    {
        $request->validate([
            'password'              => 'required|min:8',
            'password_confirmation' => 'same:password'
        ]);

        $user = Sentinel::findById($id);

        if (!$user) {
            abort(404);
        }

        if(($reminder = Reminder::exists($user)) && $resetCode === $reminder->code) {
            Reminder::complete($user, $resetCode , $request->password);
            return redirect('/admin/login')->with(['success' => 'Please log in with your new password']);
        }

        return redirect('/admin/login')->with(['error' => 'Something went wrong!']);
    }

    private function getUserRole($user)
    {
        return Sentinel::findById($user->id)->roles()->first()->slug;
    }

    public function postLogout(Request $request)
    {
        Sentinel::logout();

        if (session('login_as')) {
            $request->session()->forget('login_as');
        }
        return redirect('/admin/login')->with(['success' => 'Logout Successfully']);
    }
}
