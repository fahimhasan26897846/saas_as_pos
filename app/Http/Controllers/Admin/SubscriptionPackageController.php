<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusinessSubscriptionPackage;
use App\Http\Controllers\UserPermissionController;

class SubscriptionPackageController extends Controller
{
    private $userObj;

    public function showAllPackages()
    {
        $this->userObj = isset($this->userObj) ?: UserPermissionController::userInfo();

        if ($this->userObj->hasAccessTo('view_sub_pack')) {
            $data = [
                'thisUser'   => $this->userObj,
                'packages'      => $this->packageList()
            ];

            return view('admin.subscription-package-list')->with($data);
        }

        return redirect()->back();
    }

    public function createPackage()
    {
        $this->userObj = isset($this->userObj) ?: UserPermissionController::userInfo();

        if ($this->userObj->hasAccessTo('add_sub_pack')) {
            return view('admin.subscription-add-form')->with('thisUser', $this->userObj);
        }

        return redirect()->back();
    }

    private function packageList()
    {
        return BusinessSubscriptionPackage::getAllPackages();
    }
}
