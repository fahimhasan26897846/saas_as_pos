<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;

class BusinessCategoryController extends Controller
{
    private $_permissions;

    public function showAllCategories()
    {
        if ($this->hasAccessPermission('view_biz_cat')) {
            return view('admin.business-category-list')->with('permissions', $this->_permissions);
        }

        return redirect()->back()->with('permissions', $this->_permissions);
    }

    private function hasAccessPermission($permissionType)
    {
        $this->_permissions = Sentinel::check()->roles()->first()->permissions;
        return (isset($this->_permissions[$permissionType]) && $this->_permissions[$permissionType]);
    }
}
