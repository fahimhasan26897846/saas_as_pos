<?php

namespace App\Http\Controllers;

use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;

class UserPermissionController extends Controller
{
    private $permissions = false;
    private $user = null;
    private $role = null;

    public function hasAccessTo($permissionName)
    {
        return isset($this->permissions[$permissionName]) && $this->permissions[$permissionName];
    }

    public function isAdmin()
    {
        return ($this->role === 'Admin');
    }

    public function isSuperAdmin()
    {
        return ($this->role === 'SuperAdmin');
    }

    public function userDetails()
    {
        return $this->user;
    }

    public function getUserId()
    {
        return $this->userDetails()->id;
    }

    public function getUserRole()
    {
        return $this->role;
    }

    public static function getDefaultPermissionsFor($roleSlug)
    {
        return Sentinel::findRoleBySlug($roleSlug)->permissions;
    }

    public static function permissionsHas(array $permissions, $permissionName)
    {
        return (isset($permissions[$permissionName]) && $permissions[$permissionName]);
    }

    public static function userInfo()
    {
        $userInfo = new static;
        $userInfo->initUser();
        $userInfo->initRole();
        $userInfo->initPermissions();

        return $userInfo;
    }

    public function destroyUserSession(Request $request)
    {
        Sentinel::logout();

        if (session('login_as')) {
            $request->session()->forget('login_as');
        }

        $this->user = $this->role = null;
        $this->permissions = false;
    }

    private function initUser()
    {
        if (empty($this->user)) {
            $this->user = Sentinel::check();
        }

        if (!($this->user instanceof EloquentUser)) {
            $this->user = null;
            return false;
        }

        return true;
    }

    private function initRole()
    {
        if (empty($this->user)) {
            $this->role = null;
            return false;
        }

        if (empty($this->role)) {
            $this->role = $this->user->roles()->first()->slug;
        }

        return true;
    }

    private function initPermissions()
    {
        if (empty($this->user)) {
            return $this->permissions = false;
        }

        if (is_bool($this->permissions)) {
            $this->permissions = !empty($this->user->permissions) ? $this->user->permissions : $this->user->roles()->first()->permissions;
        }
    }
}
