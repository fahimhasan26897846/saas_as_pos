<?php

namespace App\Http\Controllers;

use App\Business;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinessController extends Controller
{
    public function createBusiness(){
        return view('createBusiness');
    }

    public function postCreateBusiness(Request $request){
        $objModel = new Business();
        $objModel->owner_user_id=$request->owner_id;
        $objModel->business_name=$request->business_name;
        $objModel->business_description=$request->business_description;
        $objModel->country=$request->business_country;
        $objModel->state=$request->business_state;
        $objModel->city=$request->business_city;
        $objModel->address=$request->business_address;

        $objFile =    $request->file('logo');
        $fileName=$objFile->getClientOriginalName().time();
        $objModel->business_logo=$fileName;
//        dd($fileName);
        $objModel->business_logo=$fileName;
        $objModel->current_package_id=$request->business_packages;
        $objModel->category_id=$request->business_catagory;
        $status=$objModel->save();
        @mkdir('Uploads/Business_Owner/');
//        $source = $_FILES["logo"]["tmp_name"];

        $destinationPath = 'Uploads/Business_Owner/'.$fileName;
//        move_uploaded_file($source, $destination);
     //   dd($objModel->business_logo);
        $objFile->move($destinationPath,$objFile->getClientOriginalName());
        return redirect('/dashboard');
    }

    function showAllBusinesses(){
        return view('business-list');
    }

    function showBusiness(){
        return view('business_owner.business');
    }
}
