<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Mail;
use Activation;

class UserRegistrationController extends Controller
{
    public function showRegistration()
    {
        return view('register');
    }

    public function postRegistration(Request $request)
    {
        $request->validate([
           'first_name'         => 'required',
           'last_name'          => 'required',
           'email'              => 'required|email',
           'password'           => 'required|min:8',
           'confirm_password'   => 'same:password',
           'agree'              => 'accepted'
        ]);

        if (Sentinel::findByCredentials(['email' => $request->email])) {
            return redirect()->back()->with(['error' => 'Email already exists']);
        }

        $user = Sentinel::register($request->all());
        $activation = Activation::create($user);
        $role = Sentinel::findRoleBySlug('User');
        $role->users()->attach($user);
        $this->sendActivationEmail($user, $activation->code);

        return redirect('/login')->with(['success' => 'Please check your email for account activation']);
    }

    private function sendActivationEmail($user, $code)
    {
        Mail::send('emails.activation', [
            'id'    => $user->id,
            'code'  => $code
        ], function ($message) use($user) {
            $message->to($user->email);
            $message->subject("Hello {$user->first_name}, Activate your account");
        });
    }

    public function activateUser(Request $request)
    {
        $user = Sentinel::findById($request->id);

        if (Activation::completed($user)) {
            return redirect('/login')->with(['success' => 'Your account is already activated. Please log in.']);
        }

        if (Activation::complete($user, $request->code)) {
            return redirect('/login')->with(['success' => 'Your account has been activated. Please log in.']);
        }

        return redirect('/register')->with(['error' => 'Something went wrong ! Please register again.']);
    }
}
