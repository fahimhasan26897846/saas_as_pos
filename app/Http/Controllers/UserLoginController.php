<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Reminder;
use Mail;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

class UserLoginController extends Controller
{
    public function showLogin()
    {
        return view('login');
    }

    private function authenticateUser(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        $user = Sentinel::findByCredentials(['email' => $request->email]);

        if ($user->roles()->first()->slug !== 'User') {
            $permissions = ($user->permissions) ?: ($user->roles()->first()->permissions);

            if (!isset($permissions['login_as_user']) || !$permissions['login_as_user']) {
                return false;
            }
        }

        if (isset($request->remember_me) && $request->remeber_me === 'on') {
            return Sentinel::authenticateAndRemember($credentials);
        }

        return Sentinel::authenticate($credentials);
    }

    public function postLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        try {
            if (!$this->authenticateUser($request)) {
                return redirect()->back()->with(['error' => 'Wrong login information.']);
            }

            $request->session()->put('login_as', 'user');
            return redirect('/dashboard');
        } catch(ThrottlingException $e) {
            $delay = $e->getDelay();
            return redirect()->back()->with(['error' => "You can login after $delay seconds."]);
        } catch(NotActivatedException $e) {
            return redirect()->back()->with(['error' => 'Account not activated yet.']);
        }
    }

    public function forgotPassword(Request $request)
    {
        $user = Sentinel::findByCredentials(['email' => $request->email]);

        if (count($user) === 1) {
            $reminder = Reminder::exists($user) ?: Reminder::create($user);
            $this->sendResetEmail($user, $reminder->code);
        }

        return redirect()->back()->with(['success' => 'Reset code was sent to your email']);
    }

    public function showResetPassword(Request $request) {
        $user = Sentinel::findById($request->id);

        if (!$user) {
            abort(404);
        }

        if(($reminder = Reminder::exists($user)) && $request->code === $reminder->code) {
            return view('reset-password')->with(['id' => $request->id, 'code' => $request->code]);
        }

        return redirect('/login');
    }

    private function sendResetEmail($user , $code)
    {
        Mail::send('emails.reset-password', [
            'id' => $user->id,
            'code' => $code
        ], function($message) use ($user) {
            $message->to($user->email);
            $message->subject("Hello $user->first_name, reset your password");
        });
    }

    public function postResetPassword(Request $request, $id, $resetCode)
    {
        $request->validate([
            'password'              => 'required|min:8',
            'password_confirmation' => 'same:password'
        ]);

        $user = Sentinel::findById($id);

        if (!$user) {
            abort(404);
        }

        if(($reminder = Reminder::exists($user)) && $resetCode === $reminder->code) {
            Reminder::complete($user, $resetCode , $request->password);
            return redirect('/login')->with(['success' => 'Please log in with your new password']);
        }

        return redirect('/login')->with(['error' => 'Something went wrong!']);
    }

    public function postLogout(Request $request)
    {
        Sentinel::logout();

        if (session('login_as')) {
            $request->session()->forget('login_as');
        }

        return redirect('/login');
    }
}
