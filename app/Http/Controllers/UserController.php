<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Expense;
use App\ExpenseCategory;
use App\Invoice;
use App\PaymentPaypal;
use App\PaymentStripe;
use App\Product;
use App\ProductCategory;
use App\Suppliers;
use App\Tax;
use App\Unit;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\View;
use Sentinel;

class UserController extends Controller
{
    public function editProfile(){

        return view('profile_setting');

    }
    public function editUpdate(Request $request){
        $objUserModel = new User();
        $userObj = $objUserModel->find($request->id);
        $userObj->email=$request->email;
        $userObj->first_name=$request->first_name;
        $userObj->last_name=$request->last_name;
        $userObj->phone=$request->phone;
        $userObj->street=$request->street;
        $userObj->password=$request->password;

		$userObj->state=$request->state;
		$userObj->city=$request->city;
		$userObj->country=$request->country;
		$userObj->postcode=$request->postcode;


		 $objFile =    $request->file('picture');
        $fileName=  time().$objFile->getClientOriginalName();
        $userObj->profile_picture =$fileName;

        $status=$userObj->update();
        @mkdir('Uploads/all_user_profile_picture/');
//        $source = $_FILES["logo"]["tmp_name"];

        $destinationPath = 'Uploads/all_user_profile_picture/';

        //   dd($objModel->business_logo);
        $objFile->move($destinationPath,$fileName);  //        move_uploaded_file($source, $destination);

        return redirect('/dashboard');


    }
    public function getProducts(){
        $user = Sentinel::getUser()->id;
        $category = ProductCategory::where('user_id','=',$user)->get();
        $product = Product::where('user_id',$user)->get();
        $tax = Tax::where('user_id',$user)->get();
        $unit = Unit::where('user_id',$user)->get();
        return view('subscriber.product.manage-product',compact('category','product','tax','unit'));}
    public function getProductsCategory(){
        $user = Sentinel::getUser()->id;
        $category = ProductCategory::where('user_id','=',$user)->get();
        return view('subscriber.product.manage-category',compact('category'));
    }
    public function postProductCategory(Request $request){
        $obj = new ProductCategory();
        $obj->user_id = $request->user_id;
        $obj->category_name = $request->name;
        $obj->description = $request->description;
        $result = $obj->save();
        if ($result){
            return redirect()->back()->with(['success'=>'Category Added']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function editProductCategory(Request $request){
        $id = Crypt::decrypt($request->id);
        $obj = new ProductCategory();
        $oj = $obj->find($id);
        $oj->category_name = $request->name;
        $oj->description = $request->description;
        $result = $oj->update();
        if ($result){
            return redirect()->back()->with(['success'=>'Category Updated']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function deleteSingleProductCategory($id)
    {
        $cd = Crypt::decrypt($id);
        $obj = ProductCategory::find($cd);
        $result = $obj->delete();
        if ($result) {
            return redirect()->back()->with(['success' => 'Category Deleted']);
        }
        return redirect()->back()->with(['error' => 'Something went wrong']);

    }
    public function getInvoices(){
        return view('subscriber.invoices.manage-invoices');
    }
    public function getCustomer(){
        $customer = Customer::where('user_id',Sentinel::getUser()->id)->get();
        return view('subscriber.customers.manage-customer',compact('customer'));

    }
    public function sendEmailBroadcust(){
        return view('subscriber.customers.send-email-broadcust');

    }
    public function importCsvData(){
        return view('subscriber.customers.import-csv-data');

    }
    public function updateBalance(){
        return view('subscriber.customers.update-balance');

    }
    public function suppliersList(){
        $supplier = Suppliers::where('user_id',Sentinel::getUser()->id)->get();
        return view('subscriber.people.suppliers',compact('supplier'));

    }
    public function invoiceList(){
        return view('subscriber.subscription.transection-invoice');

    }
    public function getCsvInvoices(){
        return view('subscriber.invoices.import-csv-data-invoice');
        
    }
    public function posTransection(){
        return view('subscriber.transections.pos-transection');

    }
    public function invoiceTransection(){
        return view('subscriber.transections.invoice-transection');
    }
    public function postProduct(Request $request)
    {
        $obj = new Product();
        $obj->user_id = Sentinel::getUser()->id;
        $obj->name = $request->name;
        $obj->barcode_type = $request->barcode_type;
        $obj->barcode = $request->weight;
        $obj->cost = $request->cost;
        $obj->price = $request->price;
        $obj->brand = $request->brand;
        $obj->description = $request->description;
        $obj->quantity = $request->quantity;
        $obj->type = $request->type;
        $obj->tract = $request->track;
        $obj->unit = $request->unit;
        $obj->category = $request->category;
        $obj->taxes = $request->taxes;
        if ($request->hasFile('image')) {
            $objFile = $request->file('image');
            $logoName = time() . $objFile->getClientOriginalName();
            $obj->image = $logoName;
            @mkdir('file/product/');
            $destinationPath = 'file/product/';
            $objFile->move($destinationPath, $logoName);
        }
        $result = $obj->save();
        if ($result){
            return redirect()->back()->with(['success'=>'Category Updated']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);

    }
    public function striprPayment(){
        return view('subscriber.payment.stripe');
        
    }
    public function paypalPayment(){
        return view('subscriber.payment.paypal');
        
    }
    public function deleteProduct($id){
        $cd = Crypt::decrypt($id);
        $obj = Product::find($cd);
        $result = $obj->delete();
        if ($result) {
            return redirect()->back()->with(['success' => 'Category Deleted']);
        }
        return redirect()->back()->with(['error' => 'Something went wrong']);
    }
    public function updateProdcut(Request $request){
        $id = Crypt::decrypt($request->id);
        $obj = Product::find($id);
        $obj->user_id = Sentinel::getUser()->id;
        $obj->name = $request->name;
        $obj->barcode_type = $request->barcode_type;
        $obj->barcode = $request->weight;
        $obj->cost = $request->cost;
        $obj->price = $request->price;
        $obj->brand = $request->brand;
        $obj->description = $request->description;
        $obj->quantity = $request->quantity;
        $obj->type = $request->type;
        $obj->tract = $request->track;
        $obj->unit = $request->unit;
        $obj->category = $request->category;
        $obj->taxes = $request->taxex;
        if ($request->hasFile('image')) {
            $objFile = $request->file('image');
            $logoName = time().$objFile->getClientOriginalName();
            $obj->image = $logoName;
            @mkdir('file/product/');
            $destinationPath = 'file/product/';
            $objFile->move($destinationPath, $logoName);
        }

        $result = $obj->save();
        if ($result){
            return redirect()->back()->with(['success'=>'Product Updated']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function getManageExpenses(){
        $expenses = Expense::where('user_id',Sentinel::getUser()->id)->get();
        $category = ExpenseCategory::where('user_id',Sentinel::getUser()->id)->get();
        return view('subscriber.expenses.manage-expenses',compact('expenses','category'));
    }
    public function addExpense(Request $request){
        $result = Expense::create($request->all());
        if ($result){
            return redirect()->back()->with(['success'=>'Expense Created']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function deleteExpense($id){
        $cd = Crypt::decrypt($id);
        $result = Expense::where('id',$cd)->delete();
        if ($result){
            return redirect()->back()->with(['success'=>'Expense deleted']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function editExpense(Request $request){
        $id = Crypt::decrypt($request->expense_id);
        $obj = Expense::find($id);
        $obj->name = $request->name;
        $obj->note = $request->note;
        $obj->date = $request->date;
        $obj->category = $request->category;
        $obj->amount = $request->amount;
        $obj->payment_mode = $request->payment_mode;
        $obj->reference = $request->reference;
        $result = $obj->update();
        if ($result){
            return redirect()->back()->with(['success'=>'Expense Updated']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function getExpenseCategory(){
        $category = ExpenseCategory::where('user_id',Sentinel::getUser()->id)->get();
        return view('subscriber.expenses.manage-categories',compact('category'));
    }
    public function postExpenseCategory(Request $request){
        $result = ExpenseCategory::create($request->all());
        if ($result){
            return redirect()->back()->with(['success'=>'Category created']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function deleteExpenseCategory($id){
        $cd = Crypt::decrypt($id);
        $result = ExpenseCategory::where('id',$cd)->delete();
        if ($result){
            return redirect()->back()->with(['success'=>'Item deleted']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function postSuppliers(Request $request){
        $result = Suppliers::create($request->all());
        if ($result){
            return redirect()->back()->with(['success'=>'Supplier added']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function deleteSuppliers($id){
        $result = Suppliers::where('id',Crypt::decrypt($id))->delete();
        if ($result){
            return redirect()->back()->with(['success'=>'Supplier deleted']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function postCustomer(Request $request){
        $result = Customer::create($request->all());
        if ($result){
            return redirect()->back()->with(['success'=>'Customer created']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function deleteCustomer($id){
        $result = Customer::where('id',Crypt::decrypt($id))->delete();
        if ($result){
            return redirect()->back()->with(['success'=>'Item deleted']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function postPaymentPaypal(Request $request){
        $find = PaymentPaypal::where('user_id',Sentinel::getUser()->id)->get();
        if (count($find) >= 1){
            return redirect()->back()->with(['error'=>'Almost Updated']);
        }
        else
            $result = PaymentPaypal::create($request->all());
        if ($result){
            return redirect()->back()->with(['success'=>'Account added']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function postPaymentStripe(Request $request){
        $find = PaymentStripe::where('user_id',Sentinel::getUser()->id)->get();
        if (count($find) >= 1){
            return redirect()->back()->with(['error'=>'Almost Updated']);
        }
        else
            $result = PaymentStripe::create($request->all());
        if ($result){
            return redirect()->back()->with(['success'=>'Account added']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function viewTax(){
        $tax = Tax::where('user_id',Sentinel::getUser()->id)->get();
        return view('subscriber.configuration.tax',compact('tax'));
    }
    public function units(){
        $tax = Unit::where('user_id',Sentinel::getUser()->id)->get();
        return view('subscriber.configuration.units',compact('tax'));
}
    public function transection(){
        return view('subscriber.configuration.invoice');
    }
    public function deleteTax($id){
        $cd = Crypt::decrypt($id);
        $result = Tax::where('id',$cd)->delete();
        if ($result){
            return redirect()->back()->with(['success'=>'Tax deleted']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function deleteUnit($id){
        $cd = Crypt::decrypt($id);
        $result = Unit::where('id',$cd)->delete();
        if ($result){
            return redirect()->back()->with(['success'=>'Tax deleted']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function postTax(Request $request){
        $result = Tax::create($request->all());
        if ($result){
            return redirect()->back()->with(['success'=>'Tax created']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function postUnit(Request $request){
        $result = Unit::create($request->all());
        if ($result){
            return redirect()->back()->with(['success'=>'Tax created']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
    public function getPosTerminal(){
        $products = Product::where('user_id',Sentinel::getUser()->id)->get();
        $customers = Customer::where('user_id',Sentinel::getUser()->id)->get();
        $tax = Tax::where('user_id',Sentinel::getUser()->id)->get();
        return view('subscriber.pos',compact('products','customers','tax'));
    }
    public function postCheckout(Request $request){
        $all = $request->amount_paid;
        $us = $request->total_bill;
        $note = $request->note;
        return view('subscriber.checkout',compact('all','us','note'));
    }
    public function postInvoice(Request $request){
        $result = Invoice::create($request->all());
        if ($result){
            return redirect()->back()->with(['success'=>'Invoice added']);
        }
        return redirect()->back()->with(['error'=>'Something went wrong']);
    }
}



