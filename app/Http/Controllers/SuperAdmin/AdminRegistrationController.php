<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserPermissionController;
use Sentinel;
use Mail;
use Activation;
use App\AdminRegister;

class AdminRegistrationController extends Controller
{
    private $userObj;

    public function showRegistration($token)
    {
        if (!($this->checkValidToken($token))) {
            return abort('404');
        }

        $emailCollection = AdminRegister::getEmailFor($token);

        if ($user = Sentinel::findByCredentials(['email' => $emailCollection[0]])) {
            $role = $user->roles()->first()->slug;
            if ($role === 'Admin' || $role === 'SuperAdmin') {
                return redirect()->back()->with(['error' => 'Email is already registered']);
            }

            if ($role === 'User') {
                return redirect("/promotion/admin/{$token}");
            }
        }

        return view('admin.admin-registration')->with(['email' => $emailCollection[0], 'token' => $token]);
    }

    public function postRegistration(Request $request, $token)
    {
        if (Sentinel::check()) { Sentinel::logout(); }

        $request->validate([
            'first_name'        => 'required',
            'last_name'         => 'required',
            'email'             => 'required|email',
            'password'          => 'required|min:8',
            'confirm_password'  => 'same:password',
        ]);

        if (AdminRegister::checkRegistrationPendingFor($request->email, $token) < 1) {
            return abort('404');
        }

        if ($user = Sentinel::findByCredentials(['email' => $request->email])) {
            $role = $user->roles()->first()->slug;
            if ($role === 'Admin' || $role === 'SuperAdmin') {
                return redirect()->back()->with(['error' => 'Email is already registered as Admin']);
            }

            if ($role === 'User') {
                return redirect("/promotion/admin/{$token}");
            }
        }

        $role = Sentinel::findRoleBySlug('Admin');
        $permissions = $role->permissions;

        if ($request->permissions && $request->permissions['login_as_user'] === 'on') {
            $permissions['login_as_user'] = true;
        }

        $user = Sentinel::register($request->all());
        $user->permissions = $permissions;
        $user->save();

        $activation = Activation::create($user);

        $role->users()->attach($user);
        $this->sendActivationEmail($user, $activation->code);

        AdminRegister::completeRegistrationFor($request->email, $token);

        return redirect('/admin/login')->with(['success' => 'Please check your email for account activation']);
    }

    public function showUserPromotion($token)
    {
        if (!($this->checkValidToken($token))) {
            return abort('404');
        }

        $emailCollection = AdminRegister::getEmailFor($token);

        if (!($user = Sentinel::findByCredentials(['email' => $emailCollection[0]]))) {
            return redirect("/register/admin/{$token}");
        }

        $role = $user->roles()->first()->slug;
        if ($role === 'Admin' || $role === 'SuperAdmin') {
            return redirect()->back()->with('error', 'Email is already registered as Admin');
        }

        if ($role !== 'User') {
            return redirect()->back()->with('error', 'Something went wrong.');
        }

        return view('admin.admin-promotion')->with(['thisUser' => $user, 'token' => $token]);
    }

    public function postUserPromotion(Request $request, $token)
    {
        if (Sentinel::check()) { Sentinel::logout(); }

        $request->validate([
            'email'     =>  'required|email',
            'password'  => 'required',
            'agree'     => 'required'
        ]);

        if (AdminRegister::checkRegistrationPendingFor($request->email, $token) < 1) {
            return abort('404');
        }

        if (!($user = Sentinel::findByCredentials(['email' => $request->email]))) {
            return redirect()->back()->with('error', 'Email is not valid for your token. Please use correct email');
        }

        if (!(Sentinel::validateCredentials($user, ['email' => $request->email, 'password' => $request->password]))) {
            return redirect()->back()->with('error', 'Email/Password does not match');
        }

        $currentRole = $user->roles()->first()->slug;
        if ($currentRole === 'Admin' || $currentRole === 'SuperAdmin') {
            return redirect()->back()->with('error', 'Email is already registered as Admin');
        }

        if ($currentRole !== 'User') {
            return redirect()->back()->with('error', 'Something went wrong.');
        }

        $role = Sentinel::findRoleBySlug('Admin');
        $permissions = $role->permissions;

        if ($request->permissions && $request->permissions['login_as_user'] === 'on') {
            $permissions['login_as_user'] = true;
        }

        $user->permissions = $permissions;
        $user->save();
        $role->users()->attach($user);

        $role = Sentinel::findRoleBySlug('User');
        $role->users()->detach($user);

        AdminRegister::completeRegistrationFor($request->email, $token);

        return redirect('/admin/login')->with(['success' => 'Congratulations. Now you can login as an Admin']);
    }

    public function showLinkGenerateForm()
    {
        $this->userObj = isset($this->userObj) ?: UserPermissionController::userInfo();

        if (!$this->userObj->hasAccessTo('add_admin')) {
            return redirect()->back();
        }

        return view('super_admin.admin-register')->with('thisUser', $this->userObj);
    }

    public function postLinkGenerateForm(Request $request)
    {
        $this->userObj = isset($this->userObj) ?: UserPermissionController::userInfo();

        if (!$this->userObj->hasAccessTo('add_admin')) {
            return redirect()->back();
        }

        $request->validate([
            'email'         => 'required|email',
            'confirm_email' => 'same:email',
            'validity'      => 'required|numeric',
        ]);

        if (!in_array($request->validity, [1, 3, 6, 12, 24])) {
            return redirect()->back()->with('error', 'Something went wrong. Please try again.');
        }

        if (AdminRegister::checkRegistrationPendingFor($request->email) >= 1) {
            return redirect()->back()->with('error', 'Registration is already pending.');
        }

        if ($user = Sentinel::findByCredentials(['email' => $request->email])) {
            $role = $user->roles()->first()->slug;
            if ($role === 'SuperAdmin' || $role === 'Admin') {
                return redirect()->back()->with('error', 'This email is already registered as Admin');
            }

            if ($role === 'User') {
                $tokenInfo = AdminRegister::generateRegistrationTokenFor($request->email, (int) $request->validity);
                $this->sendPromotionEmail($user, $tokenInfo);

                return redirect('/admin')
                       ->with('success', "Registration link successfully sent to {$request->email}");
            }
        }

        $tokenInfo = AdminRegister::generateRegistrationTokenFor($request->email, (int) $request->validity);
        $this->sendFormEmail($request->email, $tokenInfo);

        return redirect('/admin')->with('success', "Registration link successfully sent to {$request->email}");
    }

    private function sendActivationEmail($user, $code)
    {
        Mail::send('emails.activation', [
            'id'    => $user->id,
            'code'  => $code
        ], function ($message) use($user) {
            $message->to($user->email);
            $message->subject("Hello {$user->first_name}, Activate your account");
        });
    }

    private function sendFormEmail($email, $tokenInfo)
    {
        Mail::send('admin.emails.registration', [
            'token'     => $tokenInfo['token'],
            'validity'  => $tokenInfo['validity']
        ], function ($message) use($email) {
            $message->to($email);
            $message->subject("Hello, Register your email account");
        });
    }

    private function sendPromotionEmail($user, $tokenInfo)
    {
        Mail::send('admin.emails.promotion', [
            'token'     => $tokenInfo['token'],
            'validity'  => $tokenInfo['validity']
        ], function ($message) use($user) {
            $message->to($user->email);
            $message->subject("Hello {$user->first_name}, Register your email account");
        });
    }

    private function checkValidToken($token)
    {
        if (AdminRegister::checkTokenExists($token, false) >= 1) {
            return true;
        }

        return false;
    }
}
