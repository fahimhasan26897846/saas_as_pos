<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!($user = Sentinel::check())) {
            return redirect('/admin/login')->with('error', 'You need to login first');
        }

        if (session('login_as') !== 'admin') {
            return redirect()->back();
        }

        $role = $user->roles()->first()->slug;
        if ($role !== 'SuperAdmin') {
            return redirect()->back();
        }

        return $next($request);
    }
}
