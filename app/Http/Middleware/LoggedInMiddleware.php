<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class LoggedInMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Sentinel::check()) {
            return redirect('/login')->with(['error' => 'You need to login first.']);
        }

        return $next($request);
    }
}
