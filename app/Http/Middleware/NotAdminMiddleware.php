<?php

namespace App\Http\Middleware;

use Sentinel;
use Closure;

class NotAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!($user = Sentinel::check())) {
            return $next($request);
        }

        if (session('login_as') === 'admin') {
            return abort('404');
        }

        switch ($user->roles()->first()->slug) {
            case 'SuperAdmin':
            case 'Admin':
                return abort('404');
            default:
                return $next($request);
        }
    }
}
