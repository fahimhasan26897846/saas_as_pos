<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BusinessSubscriptionPackage extends Model
{
    public static function getAllPackages()
    {
        return DB::table('business_subscription_packages')->get();
    }
}
