<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $fillable = ['user_id','category_name','description'];
    protected $table = 'product_categories';
}
