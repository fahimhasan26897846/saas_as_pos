<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $fillable = ['user_id','code','amount','type'];
    protected $table = 'tax';
}
