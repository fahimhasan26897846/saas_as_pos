<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    public $timestamps=false;

    protected $fillable = [
        'owner_user_id', 'business_name','business_description','country','state','city','address','current_package_id','business_logo','category_id'
    ];

}
