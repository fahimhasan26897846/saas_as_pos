<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = ['user_id','customer','note','apply_balance','allow_balance','products','quantity'];
}
