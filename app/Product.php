<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['user_id','name','image','barcode_type','barcode','weight','cost','price','brand','description','quantity','type','tract','unit','category','taxes'];
    protected $table = 'products';
}
