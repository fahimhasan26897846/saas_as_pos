<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Admin extends Model
{
    public static function getAdminList()
    {
        return DB::table('users')->join('role_users', 'users.id', '=', 'role_users.user_id')
                                 ->join('roles', 'roles.id', '=', 'role_users.role_id')
                                 ->where('roles.slug', 'SuperAdmin')
                                 ->orWhere('roles.slug', 'Admin')
                                 ->select('users.id as id',
                                          'users.email as email',
                                          'users.first_name as first_name',
                                          'users.last_name as last_name',
                                          'roles.slug as slug',
                                          'roles.name as role')
                                 ->get();
    }
}
