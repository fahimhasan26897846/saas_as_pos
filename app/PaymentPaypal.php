<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentPaypal extends Model
{
    protected $fillable = ['user_id','mode','client_id','client_secret','enable_payment','currency'];


    protected $table = 'payments_paypals';
}
