<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suppliers extends Model
{
    protected $fillable = ['user_id','name','company','location','email','zip','city','phone','country','gst_number'];
    protected $table = 'suppliers';
}
