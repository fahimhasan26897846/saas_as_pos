<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminRegister extends Model
{
    private function createTokenRecordFor($email, $token, $validity)
    {
        $time = time();
        $validityInSeconds = (int) $validity * 60 * 60;

        DB::table('admin_registration_tokens')->insert([
                'email' => $email,
                'token' => $token,
                'token_generation_timestamp' => strftime('%G-%m-%d %H:%M:%S', $time),
                'token_expiry_timestamp' => strftime('%G-%m-%d %H:%M:%S', ($time + $validityInSeconds))
            ]);
    }

    public static function generateRegistrationTokenFor($email, $linkValidity)
    {
        $token = uniqid();

        if (static::checkTokenExists($token) >= 1) {
            return static::generateRegistrationTokenFor($email, $linkValidity);
        }

        $generateToken = new static;
        $generateToken->createTokenRecordFor($email, $token, (int) $linkValidity);

        return ['token' => $token, 'validity' => $linkValidity];
    }

    public static function checkRegistrationPendingFor($email, $token = null)
    {
        $params = [
            ['email', '=', $email],
            ['token_expiry_timestamp', '>', strftime('%G-%m-%d %H:%M:%S', time())],
            ['completed', '=', false]
        ];

        if (is_string($token)) {
            $params[] = ['token', '=', $token];
        }

        return DB::table('admin_registration_tokens')->where($params)->count();
    }

    public static function checkTokenExists($token, $completed = null)
    {
        $tableParams = [
            ['token', '=', $token],
            ['token_expiry_timestamp', '>', strftime('%G-%m-%d %H:%M:%S', time())]
        ];

        if (is_bool($completed)) { $tableParams[] = ['completed', '=', $completed]; }

        return DB::table('admin_registration_tokens')->where($tableParams)->count();
    }

    public static function getEmailFor($token)
    {
        return DB::table('admin_registration_tokens')->where([
            ['token', '=', $token],
            ['completed', '=', false],
            ['token_expiry_timestamp', '>', strftime('%G-%m-%d %H:%M:%S', time())]
        ])->pluck('email');
    }

    public static function completeRegistrationFor($email, $token)
    {
        DB::table('admin_registration_tokens')->where([
            ['email', '=', $email],
            ['token', '=', $token]
        ])->update(['completed' => true]);
    }

    public static function promoteUserToAdmin($userId)
    {
        $roles = DB::table('roles')->where('roles.slug', 'Admin')->pluck('id');
        $roleId = $roles[0];

        DB::table('role_users')->where('role_users.user_id', $userId)->update(['role_users.role_id' => $roleId]);
    }
}
