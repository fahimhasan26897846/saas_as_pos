<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = ['user_id','name','note','date','category','amount','payment_mode','reference'];
}
