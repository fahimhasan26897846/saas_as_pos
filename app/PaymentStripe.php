<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentStripe extends Model
{
    protected $fillable = ['user_id','client_id','client_secret','enable_payment','currency'];

    protected $table = 'payments_stripes';
}
