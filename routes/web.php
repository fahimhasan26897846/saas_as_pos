<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*...............INSTALLATION............*/
Route::get('/install', 'Install\InstallationController@register');
Route::post('/install', 'Install\InstallationController@postRegister');
/*................END INSTALLATION.......*/
Route::get('/', function () { return view('welcome'); });

Route::group(['middleware'=>'visitor'],function () {
    Route::get('/admin/login', 'Admin\AdminLoginController@showLogin'); // done
    Route::post('/admin/login', 'Admin\AdminLoginController@postLogin'); // done
    Route::post('/admin/reset-password', 'Admin\AdminLoginController@forgotPassword'); // done
    Route::get('/admin/{id}/reset-password/{code}', 'Admin\AdminLoginController@showResetPassword'); // done
    Route::put('/admin/{id}/reset-password/{code}', 'Admin\AdminLoginController@postResetPassword'); // done
    Route::get('/login', 'UserLoginController@showLogin'); // done
    Route::post('/login', 'UserLoginController@postLogin'); // done
    Route::post('/reset-password', 'UserLoginController@forgotPassword'); // done
    Route::get('/user/{id}/reset-password/{code}', 'UserLoginController@showResetPassword'); // done
    Route::put('/user/{id}/reset-password/{code}', 'UserLoginController@postResetPassword'); // done
    Route::get('/register', 'UserRegistrationController@showRegistration'); // done
    Route::post('/register', 'UserRegistrationController@postRegistration'); // done
    Route::get('/user/{id}/activate/{code}', 'UserRegistrationController@activateUser'); // done
});

Route::group(['middleware'=>'admin'],function () {
    Route::get('/profile-admin', 'Admin\AdminController@showProfile');
    Route::get('/edit-admin', 'Admin\AdminController@editProfile');
    Route::get('/subscription-package', 'Admin\SubscriptionPackageController@showAllPackages');
    Route::get('/subscription-package/add', 'Admin\SubscriptionPackageController@createPackage');
    Route::get('/subscription-package/{id}', 'Admin\SubscriptionPackageController@showPackage');
    Route::get('/subscription-package/{id}/edit', 'Admin\SubscriptionPackageController@editPackage');
    Route::patch('/subscription-package/{id}', 'Admin\SubscriptionPackageController@patchPackage');
    Route::post('/subscription-package', 'Admin\SubscriptionPackageController@postPackage');
    Route::delete('/subscription-package/delete', 'Admin\SubscriptionPackageController@deletePackage');
    Route::get('/business-category', 'Admin\BusinessCategoryController@showAllCategories');
    Route::get('/business-category/add', 'Admin\BusinessCategoryController@createCategory');
});

Route::group(['middleware'=>'logged_in'],function () {
    Route::post('/admin/logout', 'Admin\AdminLoginController@postLogout'); // done
    Route::post('/logout', 'UserLoginController@postLogout'); // done
    Route::get('/dashboard', 'DashboardController@showDashboard'); // working
    Route::get('/user/{id}', 'UserController@showUser');
    Route::get('/user-settings','UserController@editProfile');
    Route::post('/settings-update','UserController@editUpdate');
    Route::get('/business-category/{id}', 'Admin\BusinessCategoryController@showCategory');
    Route::get('/business-list', 'BusinessController@showAllBusinesses');
    Route::get('/business', 'BusinessController@showBusiness');
    Route::get('/business/{id}', 'BusinessController@showBusiness');
    Route::get('/business/{id}/service', 'BusinessServiceController@showAllServices');
    Route::get('/business/{id}/service/{service_id}', 'BusinessServiceController@showService');
    Route::get('/create-business', 'BusinessController@createBusiness');
    Route::post('/create-business', 'BusinessController@postCreateBusiness');
    Route::get('/manage-product','UserController@getProducts');
    Route::get('/manage-category','UserController@getProductsCategory');
    Route::post('/post-product-category','UserController@postProductCategory');
    Route::post('/updateproductcategory','UserController@editProductCategory');
    Route::get('/delete-product-category/{id}','UserController@deleteSingleProductCategory');
    Route::get('/manage-invoices','UserController@getInvoices');
    Route::get('/import-csv-data-invoice','UserController@getCsvInvoices');
    Route::get('/manage-customer','UserController@getCustomer');
    Route::get('/send-email-broadcust','UserController@sendEmailBroadcust');
    Route::get('/import-csv-data','UserController@importCsvData');
    Route::get('/update-balance','UserController@updateBalance');
    Route::get('/suppliers','UserController@suppliersList');
    Route::get('/transection-invoice','UserController@invoiceList');
    Route::get('/pos-transection','UserController@posTransection');
    Route::get('/invoice-transection','UserController@invoiceTransection');
    Route::post('/add-product','UserController@postProduct');
    Route::get('/delete-product/{id}','UserController@deleteProduct');
    Route::post('/updateproduct','UserController@updateProdcut');
    Route::get('/manage-expense','UserController@getManageExpenses');
    Route::post('/add-expense','UserController@addExpense');
    Route::get('/delete-expense/{id}','UserController@deleteExpense');
    Route::post('/updateexpense','UserController@editExpense');
    Route::get('/manage-expense-category','UserController@getExpenseCategory');
    Route::post('/add-expense-category','UserController@postExpenseCategory');
    Route::get('/delete-expense-category/{id}','UserController@deleteExpenseCategory');
    Route::post('/post-suppliers','UserController@postSuppliers');
    Route::get('/delete-supplier/{id}','UserController@deleteSuppliers');
    Route::get('/stripe','UserController@striprPayment');
    Route::get('/paypal','UserController@paypalPayment');
    Route::post('/post-customer','UserController@postCustomer');
    Route::get('/delete-customer/{id}','UserController@deleteCustomer');
    Route::post('/post-payment-paypal','UserController@postPaymentPaypal');
    Route::post('/post-payment-stripe','UserController@postPaymentStripe');
    Route::get('/Tax','UserController@viewTax');
    Route::get('/units','UserController@units');
    Route::get('/transection','UserController@transection');
    Route::get('/delete-tax/{id}','UserController@deleteTax');
    Route::post('/post-tax','UserController@postTax');
    Route::post('/post-unit','UserController@postUnit');
    Route::get('/delete-unit/{id}','UserController@deleteUnit');
    Route::get('/post-terminal','UserController@getPosTerminal');
    Route::post('/post-checkout','UserController@postCheckout');
    Route::post('/post-inoice','UserController@postInvoice');
});

Route::group(['middleware'=>'super_admin'],function () {
    Route::get('/admin/register/link', 'SuperAdmin\AdminRegistrationController@showLinkGenerateForm');
    Route::post('/admin/register/link', 'SuperAdmin\AdminRegistrationController@postLinkGenerateForm');
});

Route::group(['middleware'=>'not_admin'],function (){
    Route::get('/register/admin/{token}', 'SuperAdmin\AdminRegistrationController@showRegistration');
    Route::post('/register/admin/{token}', 'SuperAdmin\AdminRegistrationController@postRegistration');
    Route::get('/promote/admin/{token}', 'SuperAdmin\AdminRegistrationController@showUserPromotion');
    Route::post('/promote/admin/{token}', 'SuperAdmin\AdminRegistrationController@postUserPromotion');
});

Route::group(['middleware'=>'admin'],function (){
    Route::get('/admin', 'Admin\AdminController@showAdminList');
    Route::get('/admin/{id}', 'Admin\AdminController@showAdmin');
    Route::get('/admin/{id}/edit', 'Admin\AdminController@editAdmin');
    Route::patch('/admin/{id}', 'Admin\AdminController@patchAdmin');
    Route::delete('/admin/delete', 'Admin\AdminController@deleteAdmin');
    Route::get('/user', 'UserController@showAllUsers');
    Route::post('/business-category', 'Admin\BusinessCategoryController@postCategory');
    Route::get('/business-category/{id}/edit', 'Admin\BusinessCategoryController@editCategory');
    Route::patch('/business-category/{id}', 'Admin\BusinessCategoryController@patchCategory');
    Route::delete('/business-category', 'Admin\BusinessCategoryController@deleteCategory');

});


// Admin Dashboard General Setting
Route::get('/general-settings', function () {
    return view('admin.general-settings');
});


