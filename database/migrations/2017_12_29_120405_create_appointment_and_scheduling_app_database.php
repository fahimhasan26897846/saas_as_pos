<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentAndSchedulingAppDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_subscription_packages', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('package_name', 50);
            $table->text('package_description');
            $table->text('package_features');
            $table->text('package_price_validity');
            $table->text('package_permissions');
            $table->timestamps();
        });

        Schema::create('admin_registration_tokens', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('email', 50);
            $table->string('token', 25);
            $table->timestamp('token_generation_timestamp');
            $table->timestamp('token_expiry_timestamp')->nullable();
            $table->boolean('completed');
            $table->timestamps();
        });

        Schema::create('products', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('image',900)->nullable();
            $table->string('barcode_type',500)->nullable();
            $table->string('barcode')->nullable();
            $table->string('weight')->nullable();
            $table->string('cost')->nullable();
            $table->string('price')->nullable();
            $table->string('brand')->nullable();
            $table->string('description')->nullable();
            $table->string('quantity')->nullable();
            $table->string('type')->nullable();
            $table->string('tract')->nullable();
            $table->string('unit')->nullable();
            $table->string('category')->nullable();
            $table->string('taxes')->nullable();
            $table->timestamps();




        });

        Schema::create('product_categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('category_name',900)->nullable();
            $table->string('description',500)->nullable();
            $table->timestamps();
        });

        Schema::create('expenses', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('note',900)->nullable();
            $table->string('date',500)->nullable();
            $table->string('category')->nullable();
            $table->string('amount')->nullable();
            $table->string('payment_mode')->nullable();
            $table->string('reference')->nullable();
            $table->timestamps();
        });

        Schema::create('invoices', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('customer')->nullable();
            $table->string('note',900)->nullable();
            $table->string('apply_balance',500)->nullable();
            $table->string('allow_partial')->nullable();
            $table->string('products')->nullable();
            $table->string('quantinty')->nullable();
            $table->timestamps();
        });

        Schema::create('customers', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('location',900)->nullable();
            $table->string('city',500)->nullable();
            $table->string('email')->nullable();
            $table->string('zip')->nullable();
            $table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->timestamps();
        });

        Schema::create('suppliers', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('company',900)->nullable();
            $table->string('location',500)->nullable();
            $table->string('email')->nullable();
            $table->string('zip')->nullable();
            $table->string('city')->nullable();
            $table->string('phone')->nullable();
            $table->string('country')->nullable();
            $table->string('gst_number')->nullable();
            $table->timestamps();
        });

        Schema::create('payments_paypals', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('mode')->nullable();
            $table->string('client_id',900)->nullable();
            $table->string('client_secret',500)->nullable();
            $table->string('enable_payment')->nullable();
            $table->string('currency')->nullable();
            $table->timestamps();
        });

        Schema::create('payments_stripes', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('client_id',900)->nullable();
            $table->string('client_secret',500)->nullable();
            $table->string('enable_payment')->nullable();
            $table->string('currency')->nullable();
            $table->timestamps();
        });

        Schema::create('cart', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->timestamps();
        });

        Schema::create('user_subscriptions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('starting_date',100)->nullable();
            $table->string('ending_date',100)->nullable();
            $table->timestamps();
        });

        Schema::create('expense_categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name')->nullable();
            $table->string('description')->nulable();
            $table->timestamps();
        });

        Schema::create('tax', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('code')->nullable();
            $table->string('amount')->nulable();
            $table->string('type')->nulable();
            $table->timestamps();
        });

        Schema::create('unit', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name')->nullable();
            $table->string('code')->nullable();
            $table->string('base')->nulable();
            $table->string('value')->nulable();
            $table->timestamps();
        });

        Schema::create('user_invoice', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('currency')->nullable();
            $table->string('business_name')->nullable();
            $table->string('identification_number')->nulable();
            $table->string('leagal_terms')->nulable();
            $table->string('business_phone')->nulable();
            $table->string('business_location')->nulable();
            $table->string('city')->nulable();
            $table->string('country')->nulable();
            $table->string('zip_code')->nulable();
            $table->timestamps();
        });

    }

    public function down()
    {
        Schema::dropIfExists('admin_registration_tokens');
        Schema::dropIfExists('business_subscription_packages');
        Schema::dropIfExists('products');
        Schema::dropIfExists('expenses');
        Schema::dropIfExists('customers');
        Schema::dropIfExists('invoices');
        Schema::dropIfExists('cart');
        Schema::dropIfExists('tax');
        Schema::dropIfExists('unit');
        Schema::dropIfExists('user_invoice');
        Schema::dropIfExists('suppliers');
        Schema::dropIfExists('payments_paypals');
        Schema::dropIfExists('payments_stripes');
        Schema::dropIfExists('product_categories');
        Schema::dropIfExists('user_subscriptions');
        Schema::dropIfExists('expense_categories');

    }
}
