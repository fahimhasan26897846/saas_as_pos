<?php

use Illuminate\Database\Seeder;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'slug' => 'SuperAdmin','name' =>'SuperAdmin','permissions'=>'{"add_admin":true,"view_admin":true,"del_admin":true,"edit_admin_perm":true,"add_sub_pack":true,"view_sub_pack":true,"edit_sub_pack":true,"del_sub_pack":true,"view_user":true,"temp_ban_user":true,"perm_ban_user":true,"add_biz_cat":true,"view_biz_cat":true,"edit_biz_cat":true,"view_biz":true,"temp_ban_biz":true,"perm_ban_biz":true,"view_report":true,"login_as_user":true}'],
            ['id' => 2, 'slug' => 'Admin','name' =>'Admin','permissions'=>'{"add_admin":false,"view_admin":true,"del_admin":false,"edit_admin_perm":false,"add_sub_pack":false,"view_sub_pack":true,"edit_sub_pack":false,"del_sub_pack":false,"view_user":true,"temp_ban_user":true,"perm_ban_user":false,"add_biz_cat":false,"view_biz_cat":true,"edit_biz_cat":false,"view_biz":true,"temp_ban_biz":true,"perm_ban_biz":false,"view_report":true,"login_as_user":false}'],
            ['id' => 3, 'slug' => 'User','name' =>'User','permissions'=>'{"products":true,"expenses":true,"invoices":true,"Pos":true,"customers":true,"people":true,"payment":true,"transections":true,"administrations":true}'],

        ];

        foreach ($items as $item) {
            \App\Role::create($item);
        }
    }
}
