<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'email' => 'superadmin@mail.com','first_name' =>'super','last_name'=>'admin','password'=>'$2y$10$IrujXaKgg1a6Ane2LB2VSOnoXN95ZSn5d6SP8kRCPNGQWM6AVXl0G'],
            ['id' => 2, 'email' => 'admin@mail.com','first_name' =>'sub','last_name'=>'admin','password'=>'$2y$10$IrujXaKgg1a6Ane2LB2VSOnoXN95ZSn5d6SP8kRCPNGQWM6AVXl0G'],
            ['id' => 3, 'email' => 'user@mail.com','first_name' =>'normal','last_name'=>'user','password'=>'$2y$10$IrujXaKgg1a6Ane2LB2VSOnoXN95ZSn5d6SP8kRCPNGQWM6AVXl0G','permissions'=>'
{"products":true,"expenses":true,"invoices":true,"pos":true,"customers":true,"people":true,"payment":true,"transections":true,"administrations":true}'],
        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
