@extends('inc.master')

@section('title', 'SAAS APP | Payments Info')

@section('css-link')
 <!-- DataTable CSS -->
    <link href="{{asset('plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" />
@endsection

@section('content')
@include('inc.nav')
 <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Payments</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Business</a></li>
                        <li class="active">Payments</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        <!-- /row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">Customer Payments Details</h3>
                        <hr>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Patient</th>
                                        <th>Doctor</th>
                                        <th>Date</th>
                                        <th>Charges</th>
                                        <th>Discount</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Tiger Nixon</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$610</td>
                                        <td>15%</td>
                                        <td>$320</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Garrett Winters</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$630</td>
                                        <td>15%</td>
                                        <td>$170</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Ashton Cox</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$660</td>
                                        <td>15%</td>
                                        <td>$860</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Cedric Kelly</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$220</td>
                                        <td>15%</td>
                                        <td>$433</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Airi Satou</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$330</td>
                                        <td>15%</td>
                                        <td>$162</td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Brielle Williamson</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$610</td>
                                        <td>15%</td>
                                        <td>$372</td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>Herrod Chandler</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$590</td>
                                        <td>15%</td>
                                        <td>$137</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        @include('inc.footer')
@endsection

@section('script-link')
<!-- DataTable Script Link -->
 <script src="{{asset('plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
        });

</script>
@endsection
