@extends('layout.master')
@section('css-link')
 <!-- DataTable CSS -->
    <link href="{{asset('plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" />
@endsection
@section('content')
@include('SuperAdmin.inc.nav')
<div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Subscription Info</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Business</a></li>
                        <li class="active">Subscription</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        <!-- /row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">Customer Payments Details</h3>
                        <a href="#" data-toggle="modal" data-target="#responsive-modal" class="btn btn-outline btn-success pull-right" >
                            <i class="glyphicon glyphicon-plus"></i> Add Subscription Plan
                        </a>
                        <br><br>
                        <hr>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Duration</th>
                                        <th>Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Tiger Nixon</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$610</td>

                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Garrett Winters</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$630</td>

                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Ashton Cox</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$660</td>

                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Cedric Kelly</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$220</td>

                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Airi Satou</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$330</td>

                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Brielle Williamson</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$610</td>

                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>Herrod Chandler</td>
                                        <td>Steve Gection</td>
                                        <td>2011/04/25</td>
                                        <td>$590</td>

                                    </tr>
                                </tbody>
                            </table>
                        </div> <!-- end of table-responsive -->

                         <!-- /.modal -->
                        <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Add Subscription</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="">
                                                <div class="form-group">
                                                    <label for="package-title" class="control-label">Package Title:</label>
                                                    <input type="text" class="form-control" id="package-title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="package-price" class="control-label">Price:</label>
                                                    <input type="text" class="form-control" id="package-price">
                                                </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="control-label">Message:</label>
                                                    <textarea class="form-control" id="message-text"></textarea>
                                                </div>
                                                <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    @include('layout.footer')
@endsection
</div>

@section('script-link')
<!-- DataTable Script Link -->
 <script src="{{asset('plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
        });

</script>
@endsection
