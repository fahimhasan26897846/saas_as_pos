@extends('inc.master')

@section('title', 'SAAS APP | ' . session('user_info')['role'] . ' Dashboard')

@section('css-link')
<link rel="stylesheet" href="{{asset('css/dropify.min.css')}}">
@endsection

@section('content')
    @include('admin.inc.nav')

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="/dashboard">Dashboard</a></li>
                        <li class="active">Edit Admin</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">Edit Admin</h3>
                        <div>
                            <form class="form-horizontal form-material" {{-- action="/admin/{{$user->id}}" --}} method="POST">
                                {{csrf_field()}}
                                {{method_field('PUT')}}

                                <div class="form-group">
                                    <label class="col-md-12">First Name</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="" class="form-control form-control-line">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-md-12">Last Name</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input type="email" placeholder="" class="form-control form-control-line" name="example-email" id="example-email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Password</label>
                                    <div class="col-md-12">
                                        <input type="password" value="" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Phone No</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Country</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control"  name="country"></input>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">City</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control"  name="city"></input>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="col-md-12">State</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control"  name="state"></input>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="col-md-12">Street</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control"  name="street"></input>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Postal Code</label>
                                    <div class="col-md-12">
                                        <input type="number" class="form-control"  name="postal"></input>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4 ol-md-4 col-xs-12">
                                        <label for="input-file-now-custom-1">You can drag and drop a Picture / click </label>

                                            {{--<input type="hidden" name="MAX_FILE_SIZE" value="5000000">--}}
                                        <input type="file" id="input-file-now-custom-1" class="dropify" name="picture"

                                        @if(Sentinel::getUser()->profile_picture === NULL )
                                        data-default-file="{{ asset('plugins/images/default-profile-image.jpg') }}"

                                        @else

                                        data-default-file="{{ asset('Uploads/all_user_profile_picture/' . Sentinel::getUser()->profile_picture ) }}"

                                        @endif
                                        > <!-- End input -->

                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Update Profile</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

        @include('inc.footer')
    </div>
@endsection

@section('script-link')
<script src="{{asset('js/dropify.min.js')}}"> </script>
<script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify');
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>
@endsection
