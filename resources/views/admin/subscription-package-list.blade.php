@extends('inc.master')

@section('title', 'SAAS APP | ' . session('user_info')['role'] . ' Dashboard')

@section('content')

@include('admin.inc.nav')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Dashboard</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li class="active">Subscription Package Listt</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                    <h3 class="box-title">
                        Package Details
                        @if ($thisUser->hasAccessTo('add_sub_pack'))
                            <a href="/subscription-package/add" class="btn btn-outline btn-info pull-right">
                                <i class="fa fa-plus"></i> Add New Package
                            </a>
                        @endif
                    </h3>
                    <div class="table-responsive">
                        <table class="table color-table red-table table-hover">
                            <thead>
                            <tr>
                                <th>Package Name</th>
                                <th>Package Description</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($packages as $package)
                                <tr>
                                    <td><a href="/subscription-package/{{$package->id}}">{{$package->package_name}}</a></td>
                                    <td>{{$package->package_description}}</td>

                                    <td class="text-nowrap">
                                        <a href="/subscription-package/{{$package->id}}" data-toggle="tooltip" data-original-title="View">
                                            <i class="fa fa-eye text-inverse m-r-10"></i>
                                        </a>

                                        @if ($thisUser->hasAccessTo('edit_sub_pack'))
                                            <a href="/subscription-package/{{$package->id}}/edit" data-toggle="tooltip" data-original-title="Edit">
                                                <i class="fa fa-pencil text-inverse m-r-10"></i>
                                            </a>
                                        @endif

                                        @if ($thisUser->hasAccessTo('del_sub_pack'))
                                            <form action="/subscription-package/delete" method="POST" id="del-form" style="display: inline;">
                                                {{csrf_field()}}
                                                {{method_field('DELETE')}}
                                                <input type="hidden" name="user-id" value="{{$package->id}}">
                                                <a href="#" data-toggle="tooltip" data-original-title="Delete" onclick="$('#del-form').submit()">
                                                    <i class="fa fa-close text-danger"></i>
                                                </a>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    @include('inc.footer')
</div>
@endsection