@extends('inc.master')

@section('title', 'SAAS APP | ' . 'Admin Registration')

@section('content')

    <div id="page-wrapper">
        <div class="container-fluid">
            @if(session('error'))
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="fa fa-times"></i>&nbsp;&nbsp;{{session('error')}}
                </div>
            @endif
            @if(session('success'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-check"></i>&nbsp;&nbsp;{{session('success')}}
                </div>
        @endif
            <!--.row-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading"> Registration Form</div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                <form action="/promote/admin/{{$token}}" method="POST" class="form-horizontal form-bordered">
                                    {{csrf_field()}}

                                    <div class="form-body">
                                        <p>
                                        Hello {{$thisUser->first_name}}, and congratulations. We want to promote you as
                                        an admin. If you like to join our team please click on the Confirm button.
                                        </p>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email</label>
                                            <div class="col-md-9">
                                                <input type="email" value="{{$thisUser->email}}" name="email" placeholder="Email" class="form-control" required readonly>
                                                @if ($errors->has('email'))
                                                    <p style="color:#e20b0b">{{'*' . $errors->first('email')}}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Current Password</label>
                                            <div class="col-md-9">
                                                <input type="password" name="password" placeholder="Password" class="form-control" required>
                                                @if ($errors->has('password'))
                                                    <p style="color:#e20b0b">{{'*' . $errors->first('password')}}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <p>
                                        Do you want to login as normal user using this account or do you want to use this account
                                        solely for administrative tasks? For safety, we recommend you to use separate accounts
                                        for administrative and normal purposes. But if you wish to use this account for
                                        both administrative and normal purpose please enable the feature below.
                                        </p>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="checkbox checkbox-circle">
                                                    <input id="checkbox18" type="checkbox" name="permissions[login_as_user]">
                                                    <label for="checkbox18"> Can log in as user </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="checkbox checkbox-primary p-t-10">
                                                    <input id="checkbox-signup" required="" name="agree" type="checkbox">
                                                    <label for="checkbox-signup"> I agree to all <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg">Terms</a></label>
                                                </div>
                                            </div>
                                            @if ($errors->has('agree'))
                                                <p style="color:#e20b0b">{{'*' . $errors->first('agree')}}</p>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                        <button type="button" class="btn btn-default">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--./row-->
        </div>

        @include('inc.footer')
    </div>
@endsection