@extends('inc.master')

@section('title', 'SAAS APP | ' . session('user_info')['role'] . ' Dashboard')

@section('content')
    @include('admin.inc.nav')

<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Add Subscription Plan</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Subscription</a></li>
                            <li class="active">Add Subscription</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
        <!-- .row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">Add Subscription</h3>
                        <p class="text-muted m-b-30 font-13"> Add New Subscription Plan </p>
                        <form class="form">
                            <div class="form-group row">
                                <label for="subscription_title" class="col-2 col-form-label">Subscription Title</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="" id="subscription_title">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="features" class="col-2 col-form-label">Features</label>
                                <div class="col-10">
                                     <input class="form-control" type="text" value="" id="subscription_title">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Description" class="col-2 col-form-label">Description</label>
                                <div class="col-10">
                                    <textarea class="form-control" id="Description" rows="5"></textarea>
                                </div>
                            </div>

                            <div class="form-group row " >
                                <label for="subscription_plan" class="col-2 col-form-label">Price</label>

                                <div class="input-group col-10">
                                    <span class="input-group-addon">$</span>
                                    <input class="form-control" aria-label="Amount (to the nearest dollar)" type="text">
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="subscription_plan" class="col-2 col-form-label">Duration</label>

                                <div class="input-group col-4">
                                    <input type="text" class="form-control" placeholder="">
                                </div>

                                 <div class="input-group col-4">
                                    <select class="custom-select col-12" id="inlineFormCustomSelect">
                                        <option selected>Choose Duration</option>
                                        <option value="Week">Week</option>
                                        <option value="Month">Month</option>
                                        <option value="Year">Year</option>
                                    </select>
                                </div>

                                <div class="input-group col-2 ">
                                    <button class="btn btn-block btn-outline btn-info" id="add-btn">Add</button>
                                </div>
                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                                <button type="button" class="btn btn-default">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6"> </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

            @include('inc.footer')
        </div>
@endsection