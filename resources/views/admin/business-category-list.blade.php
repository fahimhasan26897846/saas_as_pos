@extends('inc.master')

@section('title', 'SAAS APP | ' . session('user_info')['role'] . ' Dashboard')

@section('content')
@include('admin.inc.nav')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Dashboard</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li class="active">Business Category List</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title">Business Category List Table</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    @include('inc.footer')
</div>
@endsection