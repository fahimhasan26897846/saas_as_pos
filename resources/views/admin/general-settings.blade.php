@extends('inc.master')

@section('title', 'SAAS APP | ' . ' Dashboard')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        @if(session('error'))
            <div class="alert alert-danger">
                {{session('error')}}
            </div>
        @endif
        @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif

        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">General Setting</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li class="active">General Setting</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

         <!--.row-->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading"> Registration Form</div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <form action="/admin/register" method="POST" class="form-horizontal form-bordered">
                                {{csrf_field()}}

                                <div class="form-body">
                                    <h3 class="box-title">Personal Info</h3>
                                    <hr>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">First Name</label>
                                        <div class="col-md-9">
                                            <input type="text" name="first_name" value="{{old('first_name')}}" placeholder="First Name" class="form-control" required>
                                            @if ($errors->has('first_name'))
                                                <p style="color:#e20b0b">{{'*' . $errors->first('first_name')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Last Name</label>
                                        <div class="col-md-9">
                                            <input type="text" name="last_name" value="{{old('last_name')}}" placeholder="Last Name" class="form-control" required>
                                            @if ($errors->has('last_name'))
                                                <p style="color:#e20b0b">{{'*' . $errors->first('last_name')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Gender</label>
                                        <div class="col-md-9">
                                            <select class="form-control" name="gender" required>
                                                <option disabled selected>Select Gender</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                                <option value="other">Other</option>
                                            </select>
                                            @if ($errors->has('gender'))
                                                <p style="color:#e20b0b">{{'*' . $errors->first('gender')}}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-date-input" class="control-label col-md-3">Date of Birth</label>
                                        <div class="col-md-9">
                                            <input type="date" name="birth_date" value="{{old('birth_date')}}" class="form-control" id="example-date-input">
                                        </div>
                                    </div>

                                    <h3 class="box-title m-t-30">Address</h3>
                                    <hr>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Street</label>
                                        <div class="col-md-9">
                                            <input type="text" value="{{old('address')}}" class="form-control" name="address">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">City</label>
                                        <div class="col-md-9">
                                            <input type="text" value="{{old('city')}}" class="form-control" name="city">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">State</label>
                                        <div class="col-md-9">
                                            <input type="text" value="{{old('state')}}" class="form-control" name="state">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Country</label>
                                        <div class="col-md-9">
                                            <input type="text" value="{{old('country')}}" class="form-control" name="country">
                                        </div>
                                    </div>

                                    <h3 class="box-title m-t-30">Account Information</h3>
                                    <hr>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Email</label>
                                        <div class="col-md-9">
                                            <input type="email" value="{{old('email')}}" name="email" placeholder="Email" class="form-control" required>
                                            @if ($errors->has('email'))
                                                <p style="color:#e20b0b">{{'*' . $errors->first('email')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Confirm Email</label>
                                        <div class="col-md-9">
                                            <input type="email" name="confirm_email" placeholder="Confirm Email" class="form-control" required>
                                            @if ($errors->has('confirm_email'))
                                                <p style="color:#e20b0b">{{'*' . $errors->first('confirm_email')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Password</label>
                                        <div class="col-md-9">
                                            <input type="password" name="password" placeholder="Password" class="form-control" required>
                                            @if ($errors->has('password'))
                                                <p style="color:#e20b0b">{{'*' . $errors->first('password')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group last">
                                        <label class="control-label col-md-3">Confirm Password</label>
                                        <div class="col-md-9">
                                            <input type="password" name="confirm_password" placeholder="Confirm Password" class="form-control" required>
                                            @if ($errors->has('confirm_password'))
                                                <p style="color:#e20b0b">{{'*' . $errors->first('confirm_password')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                        <label class="control-label col-md-3">Permissions</label>
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                            <h5 class="box-title m-b-0">Admin Access Permissions</h5>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['view_admin']) && $defaultPermissions['view_admin'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox0" type="checkbox" name="permissions[view_admin]" {{$check}}>
                                                <label for="checkbox0"> Can view admin </label>
                                            </div>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['edit_admin_perm']) && $defaultPermissions['edit_admin_perm'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox1" type="checkbox" name="permissions[edit_admin_perm]" {{$check}}>
                                                <label for="checkbox1"> Can edit admin </label>
                                            </div>

                                            <hr>
                                            <h5 class="box-title m-b-0 m-t-10">Subscription Package Permissions</h5>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['view_sub_pack']) && $defaultPermissions['view_sub_pack'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox3" type="checkbox" name="permissions[view_sub_pack]" {{$check}}>
                                                <label for="checkbox3"> Can view subscription package </label>
                                            </div>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['add_sub_pack']) && $defaultPermissions['add_sub_pack'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox4" type="checkbox" name="permissions[add_sub_pack]" {{$check}}>
                                                <label for="checkbox4"> Can add subscription package </label>
                                            </div>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['edit_sub_pack']) && $defaultPermissions['edit_sub_pack'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox5" type="checkbox" name="permissions[edit_sub_pack]" {{$check}}>
                                                <label for="checkbox5"> Can edit subscription package </label>
                                            </div>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['del_sub_pack']) && $defaultPermissions['del_sub_pack'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox6" type="checkbox" name="permissions[del_sub_pack]" {{$check}}>
                                                <label for="checkbox6"> Can delete subscription package </label>
                                            </div>

                                            <hr>
                                            <h5 class="box-title m-b-0 m-t-10">Business Category Permissions</h5>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['view_biz_cat']) && $defaultPermissions['view_biz_cat'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox7" type="checkbox" name="permissions[view_biz_cat]" {{$check}}>
                                                <label for="checkbox7"> Can view business category list </label>
                                            </div>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['add_biz_cat']) && $defaultPermissions['add_biz_cat'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox8" type="checkbox" name="permissions[add_biz_cat]" {{$check}}>
                                                <label for="checkbox8"> Can add business category </label>
                                            </div>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['edit_biz_cat']) && $defaultPermissions['edit_biz_cat'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox9" type="checkbox" name="permissions[edit_biz_cat]" {{$check}}>
                                                <label for="checkbox9"> Can edit business category </label>
                                            </div>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['del_biz_cat']) && $defaultPermissions['del_biz_cat'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox10" type="checkbox" name="permissions[del_biz_cat]" {{$check}}>
                                                <label for="checkbox10"> Can delete business category </label>
                                            </div>

                                            <hr>
                                            <h5 class="box-title m-b-0 m-t-10">Business Access Permissions</h5>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['view_biz']) && $defaultPermissions['view_biz'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox11" type="checkbox" name="permissions[view_biz]" {{$check}}>
                                                <label for="checkbox11"> Can view business details </label>
                                            </div>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['temp_ban_biz']) && $defaultPermissions['temp_ban_biz'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox12" type="checkbox" name="permissions[temp_ban_biz]" {{$check}}>
                                                <label for="checkbox12"> Can ban business temporarily </label>
                                            </div>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['perm_ban_biz']) && $defaultPermissions['perm_ban_biz'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox13" type="checkbox" name="permissions[perm_ban_biz]" {{$check}}>
                                                <label for="checkbox13"> Can ban business permanently </label>
                                            </div>

                                            <hr>
                                            <h5 class="box-title m-b-0 m-t-10">User Access Permissions</h5>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['view_user']) && $defaultPermissions['view_user'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox14" type="checkbox" name="permissions[view_user]" {{$check}}>
                                                <label for="checkbox14"> Can view user details </label>
                                            </div>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['temp_ban_user']) && $defaultPermissions['temp_ban_user'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox15" type="checkbox" name="permissions[temp_ban_user]" {{$check}}>
                                                <label for="checkbox15"> Can ban user temporarily </label>
                                            </div>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['perm_ban_user']) && $defaultPermissions['perm_ban_user'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox16" type="checkbox" name="permissions[perm_ban_user]" {{$check}}>
                                                <label for="checkbox16"> Can ban user permanently </label>
                                            </div>

                                            <hr>
                                            <h5 class="box-title m-b-0 m-t-10">Other Permissions</h5>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['view_report']) && $defaultPermissions['view_report'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox17" type="checkbox" name="permissions[view_report]" {{$check}}>
                                                <label for="checkbox17"> Can view reports </label>
                                            </div>
                                            <div class="checkbox checkbox-circle">
                                                @if ($check = (isset($defaultPermissions['login_as_user']) && $defaultPermissions['login_as_user'])
                                                                ? 'checked' : '')
                                                @endif
                                                <input id="checkbox18" type="checkbox" name="permissions[login_as_user]" {{$check}}>
                                                <label for="checkbox18"> Can log in as user </label>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                    <button type="button" class="btn btn-default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--./row-->

    </div>
    <!-- /.container-fluid -->

    @include('inc.footer')
</div>
@endsection
