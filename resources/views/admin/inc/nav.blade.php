@section('side-nav-theme')
    <link href="{{asset('css/colors/default-dark.css')}}" id="theme" rel="stylesheet">
@endsection

<!-- Top Navigation -->
<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
            <i class="ti-menu"></i>
        </a>

        <div class="top-left-part">
            <a class="logo" href="#">
                <b>
                    <!--This is dark logo icon-->
                    <img src="{{ asset('plugins/images/eliteadmin-logo.png') }}" alt="home" class="dark-logo" />
                    <!--This is light logo icon-->
                    <img src="{{ asset('plugins/images/eliteadmin-logo-dark.png') }}" alt="home" class="light-logo" />
                </b>
                <span class="hidden-xs">
                    <!--This is dark logo text-->
                    <img src="{{ asset('plugins/images/eliteadmin-text.png') }}" alt="home" class="dark-logo" />
                    <!--This is light logo text-->
                    <img src="{{ asset('plugins/images/eliteadmin-text-dark.png') }}" alt="home" class="light-logo" />
                </span>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-left hidden-xs">
            <li>
                <a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light">
                    <i class="icon-arrow-left-circle ti-menu"></i>
                </a>
            </li>
            <li>
                <form role="search" class="app-search hidden-xs">
                    <input type="text" placeholder="Search..." class="form-control">
                    <a href=""><i class="fa fa-search"></i></a>
                </form>
            </li>
        </ul>

        <ul class="nav navbar-top-links navbar-right pull-right">
            <li class="dropdown">
                <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#">
                    <i class="icon-envelope"></i>
                    <div class="notify">
                        <span class="heartbit"></span><span class="point"></span>
                    </div>
                </a>
                <ul class="dropdown-menu mailbox animated bounceInDown">
                    <li>
                        <div class="drop-title">You have 4 new messages</div>
                    </li>

                    <li>
                        <div class="message-center">
                            <a href="{{URL::to('#')}}">
                                <div class="user-img">
                                    <img src="../plugins/images/users/pawandeep.jpg" alt="user" class="img-circle">
                                    <span class="profile-status online pull-right"></span>
                                </div>
                                <div class="mail-contnet">
                                    <h5>Pavan kumar</h5>
                                    <span class="mail-desc">Just see the my admin!</span>
                                    <span class="time">9:30 AM</span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="user-img">
                                    <img src="{{ asset('plugins/images/users/sonu.jpg') }}" alt="user" class="img-circle">
                                    <span class="profile-status busy pull-right"></span>
                                </div>
                                <div class="mail-contnet">
                                    <h5>Sonu Nigam</h5>
                                    <span class="mail-desc">I've sung a song! See you at</span>
                                    <span class="time">9:10 AM</span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="user-img">
                                    <img src="{{ asset('plugins/images/users/arijit.jpg') }}" alt="user" class="img-circle">
                                    <span class="profile-status away pull-right"></span>
                                </div>
                                <div class="mail-contnet">
                                    <h5>Arijit Sinh</h5>
                                    <span class="mail-desc">I am a singer!</span>
                                    <span class="time">9:08 AM</span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="user-img">
                                    <img src="{{ asset('plugins/images/users/pawandeep.jpg') }}" alt="user" class="img-circle">
                                    <span class="profile-status offline pull-right"></span>
                                </div>
                                <div class="mail-contnet">
                                    <h5>Pavan kumar</h5>
                                    <span class="mail-desc">Just see the my admin!</span>
                                    <span class="time">9:02 AM</span>
                                </div>
                            </a>
                        </div>
                    </li>

                    <li>
                        <a class="text-center" href="javascript:void(0);">
                            <strong>See all notifications</strong> <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-messages -->
            </li>

            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle waves-effect waves-light"   data-toggle="dropdown" href="#">
                    <i class="icon-note"></i>
                    <div class="notify">
                        <span class="heartbit"></span><span class="point"></span>
                    </div>
                </a>
                <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <span class="pull-right text-muted">40% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a href="#">
                            <div>
                                <p> <strong>Task 2</strong>
                                    <span class="pull-right text-muted">20% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                        <span class="sr-only">20% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <span class="pull-right text-muted">60% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <span class="pull-right text-muted">80% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        <span class="sr-only">80% Complete (danger)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a class="text-center" href="#">
                            <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-tasks -->
            </li>
            <!-- /.dropdown -->
            <!-- /.dropdown -->
        </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
</nav>
<!-- End Top Navigation -->

<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div>
                    <img src="{{URL::asset('plugins/images/users/varun.jpg')}}" alt="user-img" class="img-circle">

                </div>
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Hello {{Sentinel::getUser()->first_name}}<span class="caret"></span>
                </a>
                <ul class="dropdown-menu animated flipInY">
                    <li><a href="/profile-admin"><i class="ti-user"></i> My Profile</a></li>
                    <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="/edit-admin"><i class="ti-settings"></i> Account Setting</a></li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <form action="/admin/logout" id="admin-logout" method="post">
                            {{csrf_field()}}
                            <a href="#" onclick="document.getElementById('admin-logout').submit()" style="color: #000000;display: block;height: 100%;width: 100%;padding-left: 20px">
                                <i class="fa fa-power-off"></i> Logout
                            </a>
                        </form>
                    </li>
                </ul>
            </div>
        </div>

        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>

            <li class="nav-small-cap m-t-10">--- Application Menu</li>

            <li>
                <a href="/dashboard" class="waves-effect">
                    <i class=" icon-speedometer fa-fw" data-icon="v"></i>
                    <span class="hide-menu"> Dashboard </span>
                </a>
            </li>


            @if ($thisUser->hasAccessTo('view_admin'))
                <li>
                    <a href="/admin" class="waves-effect">
                        <i class="ti-crown  fa-fw" data-icon="v"></i>
                        <span class="hide-menu"> Admin List </span>
                    </a>
                </li>
            @endif

            @if ($thisUser->hasAccessTo('view_sub_pack'))
                <li>
                    <a href="/subscription-package" class="waves-effect">
                        <i class="ti-shopping-cart fa-fw" data-icon="v"></i>
                        <span class="hide-menu"> Subscription Packages </span>
                    </a>
                </li>
            @endif

            @if ($thisUser->hasAccessTo('view_biz_cat'))
                <li>
                    <a href="/business" class="waves-effect">
                        <i class="icon-briefcase fa-fw" data-icon="v"></i>
                        <span class="hide-menu"> Business Category List </span>
                    </a>
                </li>
            @endif

            @if ($thisUser->hasAccessTo('view_biz'))
                <li>
                    <a href="/business" class="waves-effect">
                        <i class=" icon-layers fa-fw" data-icon="v"></i>
                        <span class="hide-menu"> Business List </span>
                    </a>
                </li>
            @endif

            @if ($thisUser->hasAccessTo('view_user'))
                <li>
                    <a href="/user" class="waves-effect">
                        <i class="icon-people fa-fw" data-icon="v"></i>
                        <span class="hide-menu"> User List </span>
                    </a>
                </li>
            @endif

            @if ($thisUser->hasAccessTo('view_report'))
                <li>
                    <a href="/report" class="waves-effect">
                        <i class="ti-agenda fa-fw" data-icon="v"></i>
                        <span class="hide-menu"> View Report </span>
                    </a>
                </li>
            @endif


                <li>
                    <a href="/setting" class="waves-effect">
                        <i class="ti-settings fa-fw" data-icon="v"></i>
                        <span class="hide-menu"> Setting <span class="fa arrow"></span> <span class="label label-rouded label-custom pull-right"></span></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li> <a href="/general-settings">General Setting</a> </li>
                        <li> <a href="/terms">Terms and Condition</a> </li>
                    </ul>
                </li>

        </ul>
    </div>
</div>
<!-- Left navbar-header end -->
