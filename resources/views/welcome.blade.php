
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('plugins/images/favicon.png')}}">

    <title>Welcome</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/starterpagestyle.css')}}" rel="stylesheet">
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <header class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">SAAS APP</h3>
              <nav class="nav nav-masthead">
                <a class="nav-link" href="/admin/login">Admin</a>
                <a class="nav-link" href="/login">User Login</a>
                <a class="nav-link" href="/register">User Signup</a>
              </nav>
            </div>
          </header>

          <main role="main" class="inner cover">
            <h3 class="cover-heading">APPOINTMENT AND SCHEDULING SOFTWARE</h3>
            <p class="lead">Sofware as a service app. Project is under construction. <strong>click the button</strong>  below to get some predefined authentication user data</p>
            <p class="lead">
              <a href="#" class="btn btn-lg btn-secondary" id="design" data-toggle="modal" data-target="#exampleModal">See Manual</a>
            </p>
          </main>
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Login info</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<div class="table-responsive">
        <table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">Number</th>
      <th scope="col">Role</th>
      <th scope="col">Email</th>
      <th scope="col">Password</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Super Admin</td>
      <td>superadmin@mail.com</td>
      <td>super</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Admin</td>
      <td>admin@mail.com</td>
      <td>super</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>User</td>
      <td>user@mail.com</td>
      <td>super</td>
    </tr>
  </tbody>
</table>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
          <footer class="mastfoot">
            <div class="inner">
              <p>Copyright SAAS &copy; All rights reserved.</p>
            </div>
          </footer>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
  </body>
</html>
 