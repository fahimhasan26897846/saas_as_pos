@extends('inc.master')

@section('title', 'SAAS APP | Create Business')
@section('css-link')
    <link rel="stylesheet" href="{{asset('css/dropify.min.css')}}">
@endsection
@section('content')
@include('inc.nav')
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">

        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Dashboard</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item" ><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active">Create Business</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <form action="/create-business" method="post" class="form-horizontal" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="owner_id" value="{{Sentinel::getUser()->id}}">
                        <div class="form-group">
                            <label for="business_name">Business Name</label>
                            <input type="text" class="form-control" name="business_name" id="business_name_input" aria-describedby="emailHelp" placeholder="Business Name">
                        </div>
                        <div class="form-group">
                            <label for="business_description">Description</label>
                            <textarea name="business_description" id="business_description_info" class="form-control" cols="30" rows="10" placeholder="Write Somthing"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="country">Country</label>
                            <input type="text" class="form-control" name="business_country" id="country_name_input"  placeholder="Country loction">
                        </div>
                        <div class="form-group">
                            <label for="state">State</label>
                            <input type="text" class="form-control" id="state_name_input" name="business_state" placeholder="Business State location">
                        </div>
                        <div class="form-group">
                            <label for="City">City</label>
                            <input type="text" class="form-control" id="business_address_input" name="business_city" placeholder="Address">
                        </div>
                        <div class="form-group">
                            <label for="Address">Address</label>
                            <input type="text" class="form-control" id="business_address_input" name="business_address" placeholder="Address">
                        </div>
                        <div class="form-group">
                            <label for="category">Business Category</label>
                            <select class="form-control" name="business_catagory">
                                <option value="1">Beauty & Wellness</option>
                                <option value="2">Cleaning Services</option>
                                <option value="3">Workspace rental</option>
                                <option value="4">Personal Trainer</option>
                                <option value="5">Pert Care</option>
                                <option value="6">Therapist</option>
                            </select>
                        </div>
                        <label for="input-file-now-custom-1">Business Logo</label>
                        <div class="form-group">
                            <div class="col-lg-5 col-md-8 col-sm-10 col-xs-12">
                                <input type="file" id="input-file-now-custom-1" class="dropify" name="logo" data-default-file="{{ asset('plugins/images/default-profile-image.jpg') }}" />
                            </div>
                        </div>
                        <label for="packages">Subscription Packages:</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="business_packages" id="inlineRadio1" value="1" checked>
                            <label class="form-check-label" for="inlineRadio1">Weekly</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="packages" id="inlineRadio2" value="2">
                            <label class="form-check-label" for="inlineRadio2">Monthly</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="packages" id="inlineRadio3" value="3">
                            <label class="form-check-label" for="inlineRadio3">Yearly</label>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-outline-primary">Submit</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
    @include('inc.footer')
</div>
@endsection

@section('script-link')
    <script type= "text/javascript" src = "{{asset('js/countries.js')}}"></script>
<script src="{{asset('js/dropify.min.js')}}"> </script>
<script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify');
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>


    <script language="javascript">
        populateCountries("country", "state");
    </script>


@endsection
