@extends('inc.master')

@section('title', 'SAAS APP | ' . session('user_info')['role'] . ' Dashboard')

@section('content')
    @include('admin.inc.nav')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Admin Registration</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li class="active">Admin Registration</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <!--.row-->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading"> Send Registration Form</div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <form action="/admin/register/link" method="POST" class="form-horizontal form-bordered">
                                {{csrf_field()}}

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Email</label>
                                        <div class="col-md-9">
                                            <input type="email" value="{{old('email')}}" name="email" placeholder="Email" class="form-control" required>
                                            @if ($errors->has('email'))
                                                <p style="color:#e20b0b">{{'*' . $errors->first('email')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Confirm Email</label>
                                        <div class="col-md-9">
                                            <input type="email" name="confirm_email" placeholder="Confirm Email" class="form-control" required>
                                            @if ($errors->has('confirm_email'))
                                                <p style="color:#e20b0b">{{'*' . $errors->first('confirm_email')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Link Validity</label>
                                        <div class="col-md-9">
                                            <select class="form-control" name="validity" required>
                                                <option disabled selected>Select a time period</option>
                                                <option value="1">1 Hour</option>
                                                <option value="3">3 Hours</option>
                                                <option value="6">6 Hours</option>
                                                <option value="12">12 Hours</option>
                                                <option value="24">1 Day</option>
                                            </select>
                                            @if ($errors->has('validity'))
                                                <p style="color:#e20b0b">{{'*' . $errors->first('gender')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Send Link</button>
                                                    <button type="button" class="btn btn-default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--./row-->
    </div>

    @include('inc.footer')
</div>
@endsection

