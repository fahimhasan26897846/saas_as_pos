@section('side-nav-theme')
    <link href="{{asset('css/colors/blue.css')}}" id="theme" rel="stylesheet">
@endsection

<!-- Top Navigation -->
<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
            <i class="ti-menu"></i>
        </a>
        <div class="top-left-part">
            <a class="logo" href="#">
                <b>
                    <!--This is dark logo icon-->
                    <img src="{{ asset('plugins/images/eliteadmin-logo.png') }}" alt="home" class="dark-logo" />
                    <!--This is light logo icon-->
                    <img src="{{ asset('plugins/images/eliteadmin-logo-dark.png') }}" alt="home" class="light-logo" />
                </b>
                <span class="hidden-xs">
                    <!--This is dark logo text-->
                    <img src="{{ asset('plugins/images/eliteadmin-text.png') }}" alt="home" class="dark-logo" />
                    <!--This is light logo text-->
                    <img src="{{ asset('plugins/images/eliteadmin-text-dark.png') }}" alt="home" class="light-logo" />
                </span>
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-left hidden-xs">
            <li>
                <a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light">
                    <i class="icon-arrow-left-circle ti-menu"></i>
                </a>
            </li>
            <li>
                <form role="search" class="app-search hidden-xs">
                    <input type="text" placeholder="Search..." class="form-control">
                    <a href=""><i class="fa fa-search"></i></a>
                </form>
            </li>
        </ul>

        <ul class="nav navbar-top-links navbar-right pull-right">
            <li class=""><a href="/create-business">Add Business</a></li>
                    <div class="notify"><span class="heartbit"></span><span class="point"></span></div></a>
            <li class="dropdown">
                <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#">
                    <i class="icon-envelope"></i>
                    <div class="notify">
                        <span class="heartbit"></span><span class="point"></span></div>
                </a>
                <ul class="dropdown-menu mailbox animated bounceInDown">
                    <li>
                        <div class="drop-title">You have 4 new messages</div>
                    </li>
                    <li>
                        <div class="message-center">
                            <a href="{{URL::to('#')}}">
                                <div class="user-img">
                                    <img src="../plugins/images/users/pawandeep.jpg" alt="user" class="img-circle">
                                    <span class="profile-status online pull-right"></span>
                                </div>

                                <div class="mail-contnet">
                                    <h5>Pavan kumar</h5>
                                    <span class="mail-desc">Just see the my admin!</span>
                                    <span class="time">9:30 AM</span>
                                </div>
                            </a>

                            <a href="#">
                                <div class="user-img">
                                    <img src="{{ asset('plugins/images/users/sonu.jpg') }}" alt="user" class="img-circle">
                                    <span class="profile-status busy pull-right"></span>
                                </div>
                                <div class="mail-contnet">
                                    <h5>Sonu Nigam</h5>
                                    <span class="mail-desc">I've sung a song! See you at</span>
                                    <span class="time">9:10 AM</span>
                                </div>
                            </a>

                            <a href="#">
                                <div class="user-img">
                                    <img src="{{ asset('plugins/images/users/arijit.jpg') }}" alt="user" class="img-circle">
                                    <span class="profile-status away pull-right"></span>
                                </div>
                                <div class="mail-contnet">
                                    <h5>Arijit Sinh</h5>
                                    <span class="mail-desc">I am a singer!</span>
                                    <span class="time">9:08 AM</span>
                                </div>
                            </a>

                            <a href="#">
                                <div class="user-img">
                                    <img src="{{ asset('plugins/images/users/pawandeep.jpg') }}" alt="user" class="img-circle">
                                    <span class="profile-status offline pull-right"></span>
                                </div>
                                <div class="mail-contnet">
                                    <h5>Pavan kumar</h5>
                                    <span class="mail-desc">Just see the my admin!</span>
                                    <span class="time">9:02 AM</span>
                                </div>
                            </a>
                        </div>
                    </li>

                    <li>
                        <a class="text-center" href="javascript:void(0);">
                            <strong>See all notifications</strong> <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-messages -->
            </li>
            <!-- /.dropdown -->

            <li class="dropdown">
                <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#">
                    <i class="icon-note"></i>
                    <div class="notify">
                        <span class="heartbit"></span><span class="point"></span>
                    </div>
                </a>
                <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <span class="pull-right text-muted">40% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a href="#">
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <span class="pull-right text-muted">20% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                        <span class="sr-only">20% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a href="#">
                            <div>
                                <p> <strong>Task 3</strong>
                                    <span class="pull-right text-muted">60% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a href="#">
                            <div>
                                <p> <strong>Task 4</strong>
                                    <span class="pull-right text-muted">80% Complete</span>
                                </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        <span class="sr-only">80% Complete (danger)</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>

                    <li class="divider"></li>

                    <li>
                        <a class="text-center" href="#">
                            <strong>See All Tasks</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-tasks -->
            </li>
            <!-- /.dropdown -->

            <!-- /.dropdown -->
        </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
</nav>
<!-- End Top Navigation -->
<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div>



                   @if(Sentinel::getUser()->profile_picture === NULL )
                        <img src="{{ asset('plugins/images/default-profile-image.jpg') }}" alt="default_photo" class="img-circle">

                       @else
                      <img src="{{ asset('Uploads/all_user_profile_picture/' . Sentinel::getUser()->profile_picture ) }}" class="img-circle" alt="default_photo" >

                   @endif
                </div>
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Hello {{Sentinel::getUser()->first_name}} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu animated flipInY">
                    <li>
                        <a href="/user-settings"><i class="ti-user"></i> My Profile</a>
                    </li>
                    <li>
                        <a href="#"><i class="ti-wallet"></i> My Balance</a>
                    </li>
                    <li>
                        <a href="#"><i class="ti-email"></i> Inbox</a>
                    </li>

                    <li role="separator" class="divider"></li>

                    <li>
                        <a href="#"><i class="ti-settings"></i> Account Setting</a>
                    </li>

                    <li role="separator" class="divider"></li>

                    <li>
                        <form action="/logout" id="admin-logout" method="post">
                            {{csrf_field()}}
                            <a href="#" onclick="document.getElementById('admin-logout').submit()" style="color: #000000;display: block;height: 100%;width: 100%;padding-left: 20px">
                                <i class="fa fa-power-off"></i> Logout
                            </a>
                        </form>
                    </li>
                </ul>
            </div>
        </div>

        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>

            <li class="nav-small-cap m-t-10">--- Application Menu</li>

            <li>
                <a href="{{URL::to('/dashboard')}}" class="waves-effect">
                    <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                    <span class="hide-menu"> Dashboard </span></a>
            </li>
            @if (Sentinel::getUser()->hasAccess('products'))
            <li>
                <a href="#" class="waves-effect">
                    <i data-icon="A" class="linea-icon linea-elaborate fa-fw"></i>
                    <span class="hide-menu">Products</span>
                </a>
                <ul class="nav nav-second-level">
                    <li> <a href="/manage-product">Manage Products</a> </li>
                    <li> <a href="/manage-category">Manage Categories</a> </li>
                </ul>
            </li>
            @endif
            @if (Sentinel::getUser()->hasAccess('expenses'))
            <li>
                <a href="#" class="waves-effect">
                    <i data-icon="A" class="linea-icon linea-elaborate fa-fw"></i>
                    <span class="hide-menu">Expenses</span>
                </a>
                <ul class="nav nav-second-level">
                    <li> <a href="/manage-expense">Manage Expense</a> </li>
                    <li> <a href="/manage-expense-category">Manage Categories</a> </li>
                </ul>
            </li>
            @endif
            @if (Sentinel::getUser()->hasAccess('invoices'))
            <li>
                <a href="#" class="waves-effect">
                    <i data-icon="A" class="linea-icon linea-elaborate fa-fw"></i>
                    <span class="hide-menu">Invoices</span>
                </a>
                <ul class="nav nav-second-level">
                    <li> <a href="/manage-invoices">Manage Invoice</a> </li>
                    <li> <a href="/import-to-csv">Import to csv</a> </li>
                </ul>
            </li>
            @endif
            @if (Sentinel::getUser()->hasAccess('pos'))
            <li>
                <a href="/post-terminal" class="waves-effect">
                    <i data-icon="A" class="linea-icon linea-elaborate fa-fw"></i>
                    <span class="hide-menu">POS Terminal</span>
                </a>
            </li>
            @endif
            @if (Sentinel::getUser()->hasAccess('customers'))
            <li>
                <a href="#" class="waves-effect">
                    <i data-icon="m" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu">Customers</span>
                </a>
                <ul class="nav nav-second-level">
                    <li> <a href="/manage-customer">Manage Customers</a> </li>
                    <li> <a href="/import-csv">Import-csv data</a> </li>
                    <li> <a href="/update-quantity">Send Email Broadcast</a> </li>
                    <li> <a href="/update-balance">Update Balance</a> </li>
                </ul>
            </li>
            @endif
            @if (Sentinel::getUser()->hasAccess('people'))
            <li>
                <a href="#" class="waves-effect">
                    <i data-icon="M" class="linea-icon linea-ecommerce fa-fw"></i>
                    <span class="hide-menu">People</span>
                </a>
                <ul class="nav nav-second-level">
                    <li> <a href="/suppliers">Suppliers</a> </li>
                </ul>
            </li>
            @endif
            @if (Sentinel::getUser()->hasAccess('payment'))
            <li>
                <a href="{{URL::to('business/owner/schedule')}}" class="waves-effect">
                    <i data-icon="A" class="linea-icon linea-elaborate fa-fw"></i>
                    <span class="hide-menu">Payment</span>
                </a>
                <ul class="nav nav-second-level">
                    <li> <a href="/bank">Bank</a> </li>
                    <li> <a href="/paypal">Paypal</a> </li>
                    <li> <a href="/stripe">Stripe</a> </li>
                </ul>
            </li>
            @endif
            @if (Sentinel::getUser()->hasAccess('Transections'))
            <li>
                <a href="#" class="waves-effect">
                    <i data-icon="A" class="linea-icon linea-elaborate fa-fw"></i>
                    <span class="hide-menu">Transections</span>
                </a>
                <ul class="nav nav-second-level">
                    <li> <a href="/manage-product">POS transection</a> </li>
                    <li> <a href="/import-csv">Invoice transection</a> </li>
                </ul>
            </li>
            @endif
            <li>
                <a href="#" class="waves-effect">
                    <i data-icon="A" class="linea-icon linea-elaborate fa-fw"></i>
                    <span class="hide-menu">Configurations</span>
                </a>
                <ul class="nav nav-second-level">
                    <li> <a href="/email">Email</a> </li>
                    <li> <a href="/post">Pos</a> </li>
                    <li> <a href="/transection">Invoice</a> </li>
                    <li> <a href="/units">Units</a> </li>
                    <li> <a href="/Tax">Tax</a> </li>
                </ul>
            </li>
            <li>
                <a href="#" class="waves-effect">
                    <i data-icon="A" class="linea-icon linea-elaborate fa-fw"></i>
                    <span class="hide-menu">Subscriptions</span>
                </a>
                <ul class="nav nav-second-level">
                    <li> <a href="/transection-invoice">Transection Invoice</a> </li>
                    <li> <a href="/change-plan">Change plan</a> </li>
                    <li> <a href="/balance">Top up balance</a> </li>
                    <li> <a href="/extends-subscription">Extend subscription</a> </li>
                </ul>
            </li>
            <li>
                <a href="{{URL::to('business/owner/settings')}}" class="waves-effect">
                    <i data-icon="P" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu">Setting</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- Left navbar-header end -->
