@extends('inc.master')

@section('title', 'SAAS APP | Business')

@section('content')
    @include('inc.nav')

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Deshboard</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li class="active">Business</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

<!-- /row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Business Details</h3>
                            <div class="table-responsive">
                                <table class="table color-table primary-table">
                                    <thead>
                                        <tr>
                                            <th>Business Name</th>
                                            <th>Location</th>
                                            <th>Staff</th>
                                            <th class="text-nowrap">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Business 1 </td>
                                            <td>Dhaka</td>
                                            <td>Staff 1 </td>
                                            <td class="text-nowrap">
                                                <a href="/business" data-toggle="tooltip" data-original-title="View"> <i class="ti-eye text-inverse"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Business 1 </td>
                                            <td>Dhaka</td>
                                            <td>Staff 1 </td>
                                            <td class="text-nowrap">
                                                <a href="#" data-toggle="tooltip" data-original-title="View"> <i class="ti-eye text-inverse"></i> </a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Business 1 </td>
                                            <td>Dhaka</td>
                                            <td>Staff 1 </td>
                                            <td class="text-nowrap">
                                                <a href="#" data-toggle="tooltip" data-original-title="View"> <i class="ti-eye text-inverse"></i> </a>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.row -->
        </div>

        </div>
        <!-- /.container-fluid -->

        @include('inc.footer')
    </div>
@endsection


