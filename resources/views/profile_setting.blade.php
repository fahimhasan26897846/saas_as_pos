@extends('inc.master')

@section('title', 'SAAS APP | BusinessOwner Dashboard')
@section('css-link')

    <link rel="stylesheet" href="{{asset('css/dropify.min.css')}}">


    @endsection

@section('content')
@include('inc.nav')
<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Profile Settings</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li class="">Deshboard</li>
                        <li class="active">My Profile</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>



            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-info">

                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">

                                            <form action="/settings-update" class="form-horizontal" method="post" enctype="multipart/form-data">

                                                {{csrf_field()}}

                                                <div class="form-body">


                                                    <h3 class="box-title">Profile Picture</h3>
                                                    <hr class="m-t-0 m-b-40">
                                                    <div class="col-sm-6 ol-md-6 col-xs-12">


                                                            <label for="input-file-now-custom-1">You can drag and drop a Picture / click </label>

                                                            {{--<input type="hidden" name="MAX_FILE_SIZE" value="5000000">--}}
                                                            <input type="file" id="input-file-now-custom-1" class="dropify" name="picture"

															@if(Sentinel::getUser()->profile_picture === NULL )
															data-default-file="{{ asset('plugins/images/default-profile-image.jpg') }}"

															@else

															data-default-file="{{ asset('Uploads/all_user_profile_picture/' . Sentinel::getUser()->profile_picture ) }}"

															@endif


															>



                                                    </div>

<br>

                                                    <h3 class="box-title">Personal Info</h3>
                                                    <hr class="m-t-0 m-b-40">

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">First Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="first_name" placeholder="Type your first name" value=" {{Sentinel::getUser()->first_name}}">
                                                                    <span class="help-block"> Your first name </span> </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Last Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="last_name" placeholder="Type your last name" value=" {{Sentinel::getUser()->last_name}}">
                                                                    <span class="help-block"> Your last name </span> </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="example-email-input"  class="control-label col-md-3">Email</label>
                                                                <div class="col-md-9">
                                                                    <input class="form-control"  id="example-email-input" type="email" name="email" value=" {{Sentinel::getUser()->email}} " >
                                                                    <span class="help-block"> Type your email. </span> </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="example-password-input" class="control-label col-md-3">Password</label>
                                                                <div class="col-md-9">
                                                                    <input class="form-control" type="password" name="password" value=" {{Sentinel::getUser()->password}} " id="example-password-input">

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                    <div class="row">

                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Phone</label>
                                                                <div class="col-md-9">
                                                                    <input class="form-control" type="tel" name="phone" placeholder="ex: 1-(555)-555-5555" id="example-tel-input">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>


                                                    <h3 class="box-title">Address</h3>
                                                    <hr class="m-t-0 m-b-40">
                                                    <!--/row-->
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Country</label>
                                                            <div class="col-md-9">
                                                                <select class="form-control" id="country" name ="country">{{Sentinel::getUser()->country}}</select>


                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->


                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">City</label>
                                                                <div class="col-md-9">
                                                                    <select class="form-control"  name="city" id ="state">{{Sentinel::getUser()->city}}</select>

                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!--/span-->
												</div>

									<div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">State</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" value="{{Sentinel::getUser()->state}}" name="state">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->

                                    <!--/row-->

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">PostCode</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="postcode" value="{{Sentinel::getUser()->postcode}}">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->

                                    </div>
                                    <!--/row-->


                                <div class="row">


                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label>Street</label>
                                            <input type="text" class="form-control"  value="{{Sentinel::getUser()->street}}" name="street">
                                        </div>
                                    </div>
                                </div>

								<div class="form-group">
                                    <input type="hidden" class="form-control" value="{{Sentinel::getUser()->id}}" name="id" >
                                </div>

                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                                <button type="button" class="btn btn-default">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6"> </div>
                                </div>
                            </div>

                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



        </div>
        <!-- /.container-fluid -->
    @include('inc.footer')
    </div>
@endsection


@section('script-link')
    <script type= "text/javascript" src = "{{asset('js/countries.js')}}"></script>
<script src="{{asset('js/dropify.min.js')}}"> </script>
<script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify');
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>


    <script language="javascript">
        populateCountries("country", "state");
    </script>


@endsection
