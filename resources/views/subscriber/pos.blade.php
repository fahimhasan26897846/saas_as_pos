@extends('inc.master')

@section('title', 'SAAS APP | Manage Product')

@section('content')
    @include('inc.nav')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">POS</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li class="active">Deshboard</li>
                        <li>POS</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        @foreach($products as $pro)
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">{{$pro->name}}</div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <p>{{$pro->description}}</p> <a class="btn btn-custom m-t-10 collapseble">Get Details</a>
                                        <button class="btn btn-custom m-t-10 btn-warning cart-product" data-name="{{$pro->name}}" data-price="{{$pro->price}}" data-unit="{{$pro->unit}}" data-tax="{{$pro->taxes}}"><i class="fa fa-shopping-cart"></i></button>
                                        <div class="m-t-15 collapseblebox dn">
                                            <div class="well">
                                            <pre>
                                                Barcode : {{$pro->barcode}}
                                                Weight: {{$pro->weight}}
                                                Quantity: {{$pro->quantity}}
                                                Type: {{$pro->type}}
                                                Unit: {{$pro->unit}}
                                            </pre>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            @endforeach

                    </div>

                </div>
                <div class="col-md-6">
                    <div class="white-box">
                        <h2>CHECKOUT</h2>
                        <p>Complete sale process and print receipt</p>
                        <hr>
                        <label for="customer">Customer</label>
                        <select class="custom-select col-12" name="track" id="inlineFormCustomSelect">
                            <option selected>Choose...</option>
                            @foreach($customers as $ta)
                                <option value="{{$ta->id}}">{{$ta->name}}</option>
                            @endforeach
                        </select>
                        <hr>
                        <div class="content">

                        </div>
                            <hr>
                            <div class="text-center tockle">
                                Sub Total: ...................... <span class="initial-price"></span>
                                <br>
                                Tax : ........................... <span class="initial-tax"></span>
                                <br>
                                <hr>
                                Total:............................ <span class="total-amount"></span>
                            </div>
                        <hr>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-danger resetti"><i class="fa fa-times"></i>&nbsp;Clear</button>
                            <button type="button" class="btn btn-warning invoice" data-toggle="modal" data-target="#exampleModal2">Invoice</button>
                            <button type="button" class="btn btn-success checkout " data-toggle="modal" data-target="#exampleModal">Checkout</button>
                        </div>

                    </div>
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Check Out</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="/post-checkout" method="post" target="_blank">
                                {{csrf_field()}}
                            <div class="modal-body">
                                <div class="form-group"><label for="amount-paid">Amount Paid</label><input type="number" name="amount_paid" id="amount-paid"  class="form-control"></div>
                                <div class="form-group"><label for="detail">Detail</label><input type="text" id="detail" name="detail"
                                                                                           class="form-control"></div>
                                <div class="form-group"><label for="total bill">Total Bill</label><input type="number" name="total_bill" class="form-control"></div>
                                <div class="form-group"><label for="">Note</label><textarea name="note" id="note"
                                                                                            cols="30"
                                                                                            rows="10"
                                                                                            class="form-control"></textarea></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel2">Invoice</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="/post-inoice" method="post">
                                {{csrf_field()}}
                                <div class="modal-body">
                                    <input type="hidden" name="user_id" value="{{Sentinel::getUser()->id}}">
                                    <input type="hidden" name="customer" id="customer">
                                    <div class="form-group"><input type="text" name="note" class="form-control" placeholder="Note"></div>
                                    <div class="form-group"><label for="allow partial">Allow Partial</label>
                                        <select name="allow-partial" id="allow paetial" class="form-control">
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

        @include('inc.footer')
    </div>
@endsection
@section('script-link')
    <script>
        $(document).ready(function() {
            if($('.initial-price').is(':empty')){
                $('.invoice').prop("disabled",true);
                $('.checkout').prop("disabled",true);
            }
        });
        $('.cart-product').on('click',function (e) {
            e.preventDefault();
            var name = $(this).data("name");
            var price = $(this).data("price");
            var unit = $(this).data("unit");
            var tax = $(this).data("tax");
            $('.invoice').removeAttr("disabled");
            $('.checkout').removeAttr("disabled");
            if($('.initial-price').is(':empty')){
                $('.initial-price').html("<p>"+price+"</p>");
                $('.initial-tax').html("<p>"+tax+"</p>");
                var total = tax + price ;
                $('.total-amount').html("<p>"+total+"</p>");
            }else{
                var d =  parseInt($('.initial-price').text());
                var effort =  parseInt($('.total-amount').text());
                var f = effort + price;
                $('.initial-price').html(price + d);
                $('.total-amount').html(f);
            }
            $(".content").append("<div class='white-box' style='box-shadow: 2px 2px 2px 1px #c3c3c3'><h5>"+name+"</h5>"
                +
                "<p class='get-price'>"+price+"</p><p class='float-right'>"+unit+
                "</p><p>"+tax+
                    "</p>"+
                "<button class='btn-btn-sm btn-danger rmvo'>Remove</button></div>");
            $('.rmvo').on('click',function () {
                var divPrice = $(this).closest('.content').find('.get-price').text();
                var int = parseInt(divPrice);
                var removed = f - int ;
                var iniRemoved = d - int;
                $('.initial-price').html(iniRemoved);
                $('.total-amount').html(removed);
                var parent = $(this).parent().remove();
            })

        });
        $('#inlineFormCustomSelect').on('change',function () {
            var cuId = $(this).val();
            $('#customer').val(cuId);
        })
    </script>
@endsection