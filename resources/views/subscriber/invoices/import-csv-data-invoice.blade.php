@extends('inc.master')

@section('title', 'SAAS APP | Import CSV Data')

@section('content')
    @include('inc.nav')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Import CSV Data</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li class="active">Invoices</li>
                        <li>Import CSV Data</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Import Records </h3>
                            <p class="text-muted m-b-30"> Makes record updating faster and easier.</p>
                            <form action="#" class="dropzone">
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        

         <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">OUTPUT </h3>
                            
                            <form action="#" class="dropzone">
                                <div class="fallback">
                                    drop a csv file to begin
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
        
        <!-- /.container-fluid -->

        @include('inc.footer')
    </div>
@endsection