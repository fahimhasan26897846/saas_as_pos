@extends('inc.master')

@section('title', 'SAAS APP |Stripe API')

@section('content')
    @include('inc.nav')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Stripe API</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li class="active">Payment</li>
                        <li>Stripe</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
      

        <!--.row-->
        <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"> Stripe API</div>
                            
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <form action="/post-payment-stripe" method="post" class="form-horizontal form-bordered">
                                        <div class="form-body">
                                            {{csrf_field()}}
                                            <input type="hidden" name="user_id" value="{{Sentinel::getUser()->id}}">
                                        <div class="form-group">
                                                <label class="control-label col-md-3">Stripe Secret Key</label>
                                                <div class="col-md-9">
                                                    <input type="text"  name="client_secret" class="form-control">
                                                     </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Stripe Public Key</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="client_id"  class="form-control">
                                                     </div>
                                            </div>
                                            <h3>Configuration</h3>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Enable Payment</label>
                                                <div class="col-md-9">
                                                <input type="checkbox" name="enable_payment" checked class="js-switch" data-color="#3d3b3b" />
                    
                                                     </div>
                                            </div>

                                          <div class="form-group last">
                                                <label class="control-label col-md-3">Select Currency</label>
                                                <div class="col-md-9">
                                                    <select name="currency" class="form-control" >
                                                    <option>Select your Currency</option>
                                                            <option>USD</option>
                                                            <option>EUR</option>
                                                            <option>AUD</option>
                                                    </select>
                                                    <span class="help-block">   Your locale currency will be converted to its equivalent on the point of payment. </span>
                                                  
                                                </div>
                                            </div>
                                            
                                            
                                          
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                             </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./row-->



        </div>
        <!-- /.container-fluid -->

        @include('inc.footer')
    </div>
@endsection