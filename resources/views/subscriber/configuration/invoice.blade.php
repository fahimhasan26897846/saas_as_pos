@extends('inc.master')

@section('title', 'SAAS APP | Manage Product')

@section('content')
    @include('inc.nav')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Deshboard</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li class="active">Deshboard</li>
                        <li>Manage product</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /.container-fluid -->

        @include('inc.footer')
    </div>
@endsection