@extends('inc.master')

@section('title', 'SAAS APP | Manage Product')
@section('css-link')
    <link href="{{asset('plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @include('inc.nav')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">PRODUCTS LIST</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li class="active">Deshboard</li>
                        <li>Manage product</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary m-3" data-toggle="modal" data-target="#exampleModal">
              <i class="fa fa-plus"></i>&nbsp;  Add
            </button>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Product</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="/add-product" method="post">
                            {{csrf_field()}}
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"><input type="text" name="name" class="form-control" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-12">Image Upload</label>
                                            <div class="col-sm-12">
                                                <label class="custom-file">
                                                    <input type="file" id="file" name="image" class="custom-file-input">
                                                    <span class="custom-file-control"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-12">Barcode Type</label>
                                            <div class="col-sm-12">
                                                <select class="custom-select col-12" name="barcode_type" id="inlineFormCustomSelect">
                                                    <option selected>Choose...</option>
                                                    <option value="EAN">EAN</option>
                                                    <option value="UPC">UPC</option>
                                                    <option value="CODE128">CODE128</option>
                                                    <option value="ITF-14">ITF-14</option>
                                                    <option value="CODE39">CODE39</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group"><input type="text" name="weight" class="form-control" placeholder="Weight">
                                        </div>
                                        <div class="form-group"><input type="text" name="brand" class="form-control" placeholder="Brand">
                                        </div>
                                        <div class="form-group"><input type="number" name="price" class="form-control" placeholder="Price">
                                        </div>
                                        <div class="form-group"><input type="number" name="quantity" class="form-control" placeholder="Quantity">
                                        </div>
                                        <div class="form-group"><input type="text" name="description" class="form-control" placeholder="Description">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-sm-12">Deduct Quantity Upon Purchase?</label>
                                            <div class="col-sm-12">
                                                <select class="custom-select col-12" name="track" id="inlineFormCustomSelect">
                                                    <option selected>Choose...</option>
                                                    <option value="Yes">Yes</option>
                                                    <option value="No">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-12">Category</label>
                                            <div class="col-sm-12">
                                                <select class="custom-select col-12" name="category" id="inlineFormCustomSelect">
                                                    <option selected>Choose...</option>
                                                    @foreach($category as $cat)
                                                        <option value="{{$cat->id}}">{{$cat->category_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-12">Unit</label>
                                            <div class="col-sm-12">
                                                <select class="custom-select col-12" name="unit" id="inlineFormCustomSelect">
                                                    <option selected>Choose...</option>
                                                    @foreach($unit as $un)
                                                        <option value="{{$un->value}}">{{$un->name}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-12">Taxes</label>
                                            <div class="col-sm-12">
                                                <select class="custom-select col-12" name="taxes" id="inlineFormCustomSelect">
                                                    <option selected>Choose...</option>
                                                    @foreach($tax as $ta)
                                                        <option value="{{$ta->amount}}">{{$ta->code}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="white-box">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Product List</h3>
                            <p class="text-muted m-b-30">Set products under your specified categories.</p>
                            <div class="table-responsive">
                                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Weight</th>
                                        <th>Brand</th>
                                        <th>Price</th>
                                        <th>Barcode</th>
                                        <th>Category</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr><th>Id</th>
                                        <th>Name</th>
                                        <th>Weight</th>
                                        <th>Brand</th>
                                        <th>Price</th>
                                        <th>Barcode</th>
                                        <th>Category</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @php $id = 1; @endphp
                                    @foreach($product as $pro)
                                    <tr>
                                        <td>{{$id}}</td>
                                        <td>{{$pro->name}}</td>
                                        <td>{{$pro->weight}}</td>
                                        <td>{{$pro->brand}}</td>
                                        <td>{{$pro->price}}</td>
                                        <td>{{$pro->barcode}}</td>
                                        <td>{{$pro->category}}</td>
                                        <td>{{$pro->unit}}</td>
                                        <td>{{$pro->quantity}}</td>
                                        <td><i class="fa fa-pencil text-success mr-2" data-toggle="modal" data-target="#exampleModal-{{$id}}"></i>
                                            <div class="modal fade" id="exampleModal-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Update</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="/updateproduct" method="post">
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="id" value="{{\Illuminate\Support\Facades\Crypt::encrypt($pro->id)}}">
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group"><input type="text" name="name" class="form-control" placeholder="Name">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-12">Image Upload</label>
                                                                            <div class="col-sm-12">
                                                                                <label class="custom-file">
                                                                                    <input type="file" id="file" name="image" class="custom-file-input">
                                                                                    <span class="custom-file-control"></span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-12">Barcode Type</label>
                                                                            <div class="col-sm-12">
                                                                                <select class="custom-select col-12" name="barcode_type" id="inlineFormCustomSelect">
                                                                                    <option selected>Choose...</option>
                                                                                    <option value="EAN">EAN</option>
                                                                                    <option value="UPC">UPC</option>
                                                                                    <option value="CODE128">CODE128</option>
                                                                                    <option value="ITF-14">ITF-14</option>
                                                                                    <option value="CODE39">CODE39</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group"><input type="text" name="weight" class="form-control" placeholder="Weight">
                                                                        </div>
                                                                        <div class="form-group"><input type="text" name="brand" class="form-control" placeholder="Brand">
                                                                        </div>
                                                                        <div class="form-group"><input type="number" name="price" class="form-control" placeholder="Price">
                                                                        </div>
                                                                        <div class="form-group"><input type="number" name="quantity" class="form-control" placeholder="Quantity">
                                                                        </div>
                                                                        <div class="form-group"><input type="text" name="description" class="form-control" placeholder="Description">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="col-sm-12">Deduct Quantity Upon Purchase?</label>
                                                                            <div class="col-sm-12">
                                                                                <select class="custom-select col-12" name="track" id="inlineFormCustomSelect">
                                                                                    <option selected>Choose...</option>
                                                                                    <option value="Yes">Yes</option>
                                                                                    <option value="No">No</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-12">Category</label>
                                                                            <div class="col-sm-12">
                                                                                <select class="custom-select col-12" name="category" id="inlineFormCustomSelect">
                                                                                    <option selected>Choose...</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-12">Unit</label>
                                                                            <div class="col-sm-12">
                                                                                <select class="custom-select col-12" name="unit" id="inlineFormCustomSelect">
                                                                                    <option selected>Choose...</option>
                                                                                    @foreach($category as $cat)
                                                                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-12">Taxes</label>
                                                                            <div class="col-sm-12">
                                                                                <select class="custom-select col-12" name="track" id="inlineFormCustomSelect">
                                                                                    <option selected>Choose...</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                                            </div></form>

                                                    </div>
                                                </div>
                                            </div>
                                            <a
                                                    href="/delete-product/{{\Illuminate\Support\Facades\Crypt::encrypt($pro->id)}}"><i
                                                        class="fa fa-trash text-danger"></i></a>
                                        </td>
                                    </tr>
                                    @php $id ++; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

        @include('inc.footer')
    </div>
@endsection
@section('script-link')
    <script src="{{asset('plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
            $(document).ready(function() {
                var table = $('#example').DataTable({
                    "columnDefs": [{
                        "visible": false,
                        "targets": 2
                    }],
                    "order": [
                        [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
                // Order by the grouping
                $('#example tbody').on('click', 'tr.group', function() {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                        table.order([2, 'desc']).draw();
                    } else {
                        table.order([2, 'asc']).draw();
                    }
                });
            });
        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    </script>
    <!--Style Switcher -->
@endsection