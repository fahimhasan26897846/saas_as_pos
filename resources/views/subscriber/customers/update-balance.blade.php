@extends('inc.master')

@section('title', 'SAAS APP | Manage Product')

@section('content')
    @include('inc.nav')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Update Balance</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li class="active">Customers</li>
                        <li>Balance</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

              <!--.row-->
              <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"> Add Balance</div>
                            
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <form action="#" class="form-horizontal form-bordered">
                                        <div class="form-body">

                                          <div class="form-group last">
                                                <label class="control-label col-md-3">Customer</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" >
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Balance</label>
                                                <div class="col-md-9">
                                                    <input type="text" placeholder="Set Amount" class="form-control">
                                                     </div>
                                            </div>
                                            
                                        
                                          
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Update</button>
                                                            <button type="button" class="btn btn-default">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./row-->





            
        </div>
        <!-- /.container-fluid -->

        @include('inc.footer')
    </div>
@endsection