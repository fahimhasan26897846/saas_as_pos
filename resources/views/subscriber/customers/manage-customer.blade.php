@extends('inc.master')

@section('title', 'SAAS APP | Manage Product')
@section('css-link')
    <link href="{{asset('plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @include('inc.nav')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Deshboard</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li class="active">Deshboard</li>
                        <li>Manage Customers</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
             <!-- Button trigger modal -->
             <button type="button" class="btn btn-primary m-3" data-toggle="modal" data-target="#exampleModal">
                Add Customer
            </button>

       
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                                   <!--.row-->
                        <div class="panel panel-info">
                            <div class="panel-heading"> Add a customer</div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <form action="/post-customer" method="post" class="form-horizontal form-bordered">
                                    {{csrf_field()}}
                                    <input type="hidden" name="user_id" value="{{Sentinel::getUser()->id}}">
                                <div class="panel-body">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Full Name</label>
                                            <div class="col-md-9">
                                                <input type="text" name="name" placeholder="Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="white-box">
                                                <h3 class="box-title">ATTACH SQUARE SIZE IMAGES</h3>
                                                <label for="input-file-now-custom-1"></label>
                                                <input type="file" id="input-file-now-custom-1" class="dropify" data-default-file="../plugins/bower_components/dropify/src/images/test-image-1.jpg" />
                                            </div>




                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email</label>
                                            <div class="col-md-9">
                                                <input type="email" name="email" placeholder="Supplier Email(If available)" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Phone</label>
                                            <div class="col-md-9">
                                                <input type="number" name="phone" placeholder="Supplier Phone(If available)" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Location</label>
                                            <div class="col-md-9">
                                                <input type="text" name="location" placeholder="Enter Location (Optional)" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">City</label>
                                            <div class="col-md-9">
                                                <input type="text" name="city" placeholder="Enter City (Optional)" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Zip</label>
                                            <div class="col-md-9">
                                                <input type="text" name="zip" placeholder="Enter Zip (Optional)" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Country</label>
                                            <div class="col-md-9">
                                                <input type="text" name="city" placeholder="Enter Country(Optional)" class="form-control">
                                            </div>
                                        </div>



                                    </div>
                                        </div>
                                       

                                    <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                                </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="white-box">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Customers</h3>
                            <p class="text-muted m-b-30">List of customers and their transaction activities</p>
                            <div class="table-responsive">
                                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>city</th>
                                        <th>email</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>city</th>
                                        <th>email</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($customer as $cu)
                                    <tr>
                                        <td>{{$cu->name}}</td>
                                        <td>{{$cu->location}}</td>
                                        <td>{{$cu->city}}</td>
                                        <td>{{$cu->email}}</td>
                                        <td>{{$cu->phone}}</td>
                                        <td>
                                            <a href="/delete-customer/{{\Illuminate\Support\Facades\Crypt::encrypt($cu->id)}}"><i class="fa fa-trash text-danger"></i></a></td>
                                    </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

        @include('inc.footer')
    </div>
@endsection
@section('script-link')
    <script src="{{asset('plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
            $(document).ready(function() {
                var table = $('#example').DataTable({
                    "columnDefs": [{
                        "visible": false,
                        "targets": 2
                    }],
                    "order": [
                        [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
                // Order by the grouping
                $('#example tbody').on('click', 'tr.group', function() {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                        table.order([2, 'desc']).draw();
                    } else {
                        table.order([2, 'asc']).draw();
                    }
                });
            });
        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    </script>
    <!--Style Switcher -->
@endsection