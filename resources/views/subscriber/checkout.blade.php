@extends('inc.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="white-box text-center mt-5">
                    <h6>Your Cart Detail</h6>
                    @if(isset($all))
                    <p>Amount Paid {{$all}}</p>
                    @endif
                    @if(isset($us))
                    <p>Total Paid {{$us}}</p>
                    @endif
                    <br>
                    @if(isset($note))
                    <p>Description {{$note}}</p>
                    @endif
                    <button class="btn btn-lg btn-primary print">
                        <i class="fa fa-print"></i>&nbsp; Print Cart
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endsection
@section('script-link')
    <script>
        $('.print').on('click',function () {
            (window).print();
        })
    </script>
@endsection