@extends('inc.master')

@section('title', 'SAAS APP | Business')

@section('css-link')
<!-- Calendar CSS -->
<link href="{{asset('plugins/bower_components/calendar/dist/fullcalendar.css')}}" rel="stylesheet" />

@endsection

@section('content')
    @include('inc.nav')

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Deshboard</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li class="active">Business</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

                           <!-- .row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <!-- Tabstyle start -->
                            <h3 class="box-title m-b-0">Busines Details </h3>
                            <hr>
                            <section>
                                <div class="sttabs tabs-style-bar">
                                    <nav>
                                        <ul>
                                            <li><a href="#business" class="sticon ti-briefcase"><span>Business Details</span></a></li>
                                            <li><a href="#services" class="sticon ti-server"><span>Services</span></a></li>
                                            <li><a href="#schedules" class="sticon  ti-calendar"><span>Schedules</span></a></li>
                                            <li><a href="#create_sechedule" class="sticon ti-clipboard"><span>Create Schedult</span></a></li>
                                            <li><a href="#settings" class="sticon ti-settings"><span>Settings</span></a></li>
                                        </ul>
                                    </nav>
                                    <div class="content-wrap">
                                        <section id="business">
                                            <h3>Best Clean Tab ever</h3>
                                            <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
                                        </section>
                                        <section id="services">
                                            <h2>Tabbing 2</h2>
                                        </section>
                                        <section id="schedules">
                                            <h2>Tabbing 3</h2>
                                        </section>
                                        <section id="create_sechedule">
                                            <div class="col-md-12">
                                                <div class="white-box">
                                                    <div id="calendar"></div>
                                                </div>
                                            </div>
                                        </section>
                                        <section id="settings">
                                            <h2>Tabbing 5</h2>
                                        </section>
                                    </div>
                                    <!-- /content -->
                                </div>
                                <!-- /tabs -->
                            </section>
                            <!-- Tabstyle start -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

        @include('inc.footer')
    </div>
@endsection

@section('script-link')
<script src="{{asset('js/cbpFWTabs.js')}}"></script>
    <script type="text/javascript">
    (function() {

        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
            new CBPFWTabs(el);
        });

    })();
    </script>

 <!-- Calendar JavaScript -->
<script src="{{asset('plugins/bower_components/moment/moment.js')}}"></script>
<script src="{{asset('plugins/bower_components/calendar/dist/fullcalendar.min.js')}}"></script>
<script src="{{asset('plugins/bower_components/calendar/dist/cal-init.js')}}"></script>

@endsection
