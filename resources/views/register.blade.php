@extends('inc.master')

@section('title', 'SAAS APP | ' . 'Registration')

@section('content')
    <section id="wrapper" class="login-register">
        <div class="login-box login-sidebar">
            <div class="white-box">
                <form class="form-horizontal form-material" id="loginform" action="/register" method="post">
                    {{csrf_field()}}
                    @if(session('error'))
                        <div class="alert alert-danger alert-dismissable">
                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-times"></i>&nbsp;&nbsp;{{session('error')}}
                        </div>
                    @endif
                    @if(session('success'))
                        <div class="alert alert-success alert-dismissable msg">
                             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-check"></i>&nbsp;&nbsp;{{session('success')}}
                        </div>
                    @endif
                    <a href="javascript:void(0)" class="text-center db"><img src="{{ asset('plugins/images/eliteadmin-logo-dark.png') }}" alt="Home" />
                        <br/><img src="{{ asset('plugins/images/eliteadmin-text-dark.png') }}" alt="Home" /></a>
                    <h3 class="box-title m-t-30 m-b-0">Register Now</h3><small>Create your account and enjoy</small>
                    <div class="form-group m-t-15 m-b-15">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" name="first_name"  placeholder="First Name">
                        </div>
                        @if ($errors->has('first_name'))
                            <p style="color:#e20b0b">{{'*' . $errors->first('first_name')}}</p>
                        @endif
                    </div>
                    <div class="form-group m-t-10 m-b-15">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" name="last_name"  placeholder="Last Name">
                        </div>
                        @if ($errors->has('last_name'))
                            <p style="color:#e20b0b">{{'*' . $errors->first('last_name')}}</p>
                        @endif
                    </div>
                    <div class="form-group m-t-10 m-b-15">
                        <div class="col-xs-12">
                            <input class="form-control" type="email" required="" name="email" placeholder="Email">
                        </div>
                        @if ($errors->has('email'))
                            <p style="color:#e20b0b">{{'*' . $errors->first('email')}}</p>
                        @endif
                    </div>
                    <div class="form-group m-t-10 m-b-15">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required="" name="password" placeholder="Password">
                        </div>
                        @if ($errors->has('password'))
                            <p style="color:#e20b0b">{{'*' . $errors->first('password')}}</p>
                        @endif
                    </div>
                    <div class="form-group m-t-10 m-b-15">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required="" name="confirm_password" placeholder="Confirm Password">
                        </div>
                        @if ($errors->has('confirm_password'))
                            <p style="color:#e20b0b">{{'*' . $errors->first('confirm_password')}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary p-t-10">
                                <input id="checkbox-signup" required="" name="agree" type="checkbox">
                                <label for="checkbox-signup"> I agree to all <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg">Terms</a></label>
                            </div>
                        </div>
                        @if ($errors->has('agree'))
                            <p style="color:#e20b0b">{{'*' . $errors->first('agree')}}</p>
                        @endif
                    </div>
                    <div class="form-group text-center m-t-10 m-b-15">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Sign Up</button>
                        </div>
                    </div>

                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>Already have an account? <a href="/login" class="text-primary m-l-5"><b>Sign In</b></a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>


    <!-- Modal For Terms and Condition -->


    <!-- sample modal content -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
                </div>
                <div class="modal-body">
                    <h4>Overflowing text to show scroll behavior</h4>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                    <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection

@section('script-link')
<script>
    $('.alert').fadeIn(4000).delay(3000);
    $('.alert').fadeOut(2000);
</script>
@endsection
