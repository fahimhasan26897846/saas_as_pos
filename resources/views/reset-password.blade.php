@extends('inc.master')

@section('content')
    <section id="wrapper" class="login-register">
        <div class="login-box login-sidebar">
            <div class="white-box">
                <form class="form-horizontal form-material" id="loginform" action="/user/{{$id}}/reset-password/{{$code}}" method="POST">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>

                            @endforeach
                        </ul>
                        </div>
                    @endif
                    <a href="javascript:void(0)" class="text-center db"><img src="{{ asset('plugins/images/eliteadmin-logo-dark.png') }}" alt="Home" />
                        <br/><img src="{{ asset('plugins/images/eliteadmin-text-dark.png') }}" alt="Home" /></a>
                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="password" required="" placeholder="New Password">
                        </div>
                        @if ($errors->has('password'))
                            <p style="color:#e20b0b">{{'*' . $errors->first('password')}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="password_confirmation" required="" placeholder="Confirm Password">
                        </div>
                        @if ($errors->has('password_confirmation'))
                            <p style="color:#e20b0b">{{'*' . $errors->first('password_confirmation')}}</p>
                        @endif
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </section>
@endsection
